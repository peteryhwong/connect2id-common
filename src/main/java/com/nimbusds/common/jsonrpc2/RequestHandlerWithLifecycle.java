package com.nimbusds.common.jsonrpc2;


import com.thetransactioncompany.jsonrpc2.server.RequestHandler;


/**
 * JSON-RPC 2.0 request handler with lifecycle. Extends the basic request 
 * handler interface by providing a {@link #start} method for initialising and 
 * allocating resources, such as database connections, and a {@link #stop} 
 * method for freeing them when the handler is to be shut down.
 */
public interface RequestHandlerWithLifecycle extends RequestHandler {
	
	
	/**
	 * Starts the request handler.
	 *
	 * @throws Exception If the request handler couldn't be started.
	 */
	public void start() throws Exception;
	
	
	/**
	 * Stops the request handler.
	 *
	 * @throws Exception If an exception was encountered.
	 */
	public void stop() throws Exception;
}
