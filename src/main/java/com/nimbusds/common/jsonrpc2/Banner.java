package com.nimbusds.common.jsonrpc2;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;


/**
 * Welcome banner for Connect2id JSON-RPC 2.0 services.
 *
 * <p>Example output:
 *
 * <pre>
 * Use HTTP POST to submit your JSON-RPC 2.0 request
 *
 * Web service: LdapAuth, version 3.0.1 (2014-02-20)
 * Vendor: Connect2id Ltd., http://connect2id.com
 * 
 * Supported JSON-RPC 2.0 requests:
 *	* user.auth
 *	* user.get
 *	* ws.getName
 *	* ws.getTime
 *	* ws.getVersion
 * </pre>
 */
public class Banner {


	/**
	 * The web service name.
	 */
	private String wsName;
	
	
	/**
	 * The web service version and build date.
	 */
	private String wsVersion;
	
	
	/**
	 * The handled methods, sorted.
	 */
	private String[] handledMethods;
	
	
	/**
	 * The composed banner message.
	 */
	private String bannerMessage;
	
	
	/**
	 * Creates a new welcome banner for a Connect2id JSON-RPC 2.0 service.
	 *
	 * @param wsName    The web service name. Must not be {@code null}.
	 * @param wsVersion The web service version and build date. Must not be
	 *                  {@code null}.
	 * @param methods   The handled methods. Must not be {@code null}.
	 */
	public Banner(final String wsName,
		      final String wsVersion,
		      final String[] methods) {
	
		if (wsName == null)
			throw new IllegalArgumentException("The web service name must not be null");
		
		this.wsName = wsName;
		
		if (wsVersion == null)
			throw new IllegalArgumentException("The web service version must not be null");
		
		this.wsVersion = wsVersion;
		
		if (methods == null)
			throw new IllegalArgumentException("The handled JSON-RPC 2.0 method names must not be null");
		
		handledMethods = Arrays.copyOf(methods, methods.length);

		// Sort in place
		Arrays.sort(handledMethods);
		
		bannerMessage = composeMessage(wsName, wsVersion, handledMethods);
	}
	
	
	/**
	 * Gets the web service name.
	 *
	 * @return The web service name.
	 */
	public String getWsName() {
	
		return wsName;
	}
	
	
	/**
	 * Gets the web service version and build date.
	 *
	 * @return The web service version and build date.
	 */
	public String getWsVersion() {
	
		return wsVersion;
	}
	
	
	/**
	 * Gets the handled JSON-RPC 2.0 methods.
	 *
	 * @return The handled JSON-RPC 2.0 methods, sorted.
	 */
	public String[] getHandledJSONRPC2Methods() {
	
		return handledMethods;
	}
	
	
	/**
	 * Composes a banner message.
	 *
	 * @param wsName    The web service name. Must not be {@code null}.
	 * @param wsVersion The web service version and build date. Must not be
	 *                  {@code null}.
	 * @param methods   The handled methods. Must not be {@code null}.
	 *
	 * @return The banner message.
	 */
	public static String composeMessage(final String wsName, 
	                                    final String wsVersion,
			                    final String[] methods) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append("Use HTTP POST to submit your JSON-RPC 2.0 request\n");
		sb.append('\n');
		sb.append("Web service: " + wsName + ", version " + wsVersion + '\n');
		sb.append("Vendor: Connect2id Ltd., http://connect2id.com\n");
		sb.append('\n');
		sb.append("Supported JSON-RPC 2.0 requests:\n");

		for (String m: methods) {
			sb.append("\t* ");
			sb.append(m);
			sb.append('\n');
		}
		
		return sb.toString();
	}


	/**
	 * Applies this banner to the specified HTTP servlet response.
	 *
	 * <p>Appends a "X-Web-Service" HTTP header set to {@link #getWsName}.
	 *
	 * <p>Sets the "Content-Type" HTTP header to "text/plain;charset=utf-8".
	 *
	 * <p>Prints a message with the following format:
	 *
	 * <pre>
	 * Use HTTP POST to submit your JSON-RPC 2.0 request
	 *
	 * Web service: [ws-name], [ws-version]
	 * Vendor: Connect2id Ltd., http://connect2id.com
	 * 
	 * Supported JSON-RPC 2.0 requests:
	 *	* [method-1]
	 *      * [method-2]
	 *      * [method-x]
	 * </pre>
	 *
	 * @param httpResponse The HTTP servlet response.
	 *
	 * @throws IOException If an I/O exception was encountered.
	 */
	public void apply(final HttpServletResponse httpResponse)
		throws IOException {
		
		// Identify web service name in the HTTP header
		httpResponse.setHeader("X-Web-Service", wsName);
		
		// Indicate a plain text message
		httpResponse.setContentType("text/plain;charset=utf-8");
                
                PrintWriter out = httpResponse.getWriter();

		// Identify web service and print message to use
		// HTTP POST instead
		out.println(bannerMessage);

		out.close();
	}
}
