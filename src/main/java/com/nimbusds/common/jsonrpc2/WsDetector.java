package com.nimbusds.common.jsonrpc2;


import java.net.URL;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;


/**
 * Detects the name and version of a remote JSON-RPC 2.0 web service, as 
 * reported by a {@link WsInfoRequestHandler}.
 */
public class WsDetector {
	
	
	/**
	 * The client session.
	 */
	private final JSONRPC2Session session;
	
	
	/**
	 * Creates a new JSON-RPC 2.0 web service detector.
	 *
	 * @param url The web service HTTP(S) URL. Must not be {@code null}.
	 */
	public WsDetector(final URL url) {
	
		session = new JSONRPC2Session(url);
	}
	
	
	/**
	 * Gets the client session associated with this JSON-RPC 2.0 web service
	 * detected.
	 *
	 * @return The JSON-RPC 2.0 client session.
	 */
	public JSONRPC2Session getSession() {
	
		return session;
	}


	/**
	 * Detects the JSON-RPC 2.0 web service name and version. The target web
	 * service must handle {@code ws.getName} and {@code ws.getVersion} 
	 * calls, as implemented by {@link WsInfoRequestHandler}.
	 *
	 * @return The reported web service name and version.
	 *
	 * @throws Exception If the web service name and version couldn't be 
	 *                   detected, for example due to a network exception.
	 */
	public WsInfo detect()
		throws Exception {
		
		// Make ws.getName request
		JSONRPC2Response response;
		
		try {
			response = session.send(new JSONRPC2Request("ws.getName", 0));
			
		} catch (JSONRPC2SessionException e) {
			
			throw new Exception(e.getMessage(), e);
		}
		
		if (! response.indicatesSuccess()) {
		
			JSONRPC2Error err = response.getError();
			
			throw new Exception("ws.getName failed: [" + err.getCode() + "] " + err.getMessage(), err);
		}
		
		if (! (response.getResult() instanceof String))
			throw new Exception("Unexpected ws.getName response, must be a string");
	
		String wsName = (String)response.getResult();
		
		
		// Make ws.getVersion request
		try {
			response = session.send(new JSONRPC2Request("ws.getVersion", 0));
			
		} catch (JSONRPC2SessionException e) {
		
			throw new Exception(e.getMessage(), e);
		}
		
		if (! response.indicatesSuccess()) {
		
			JSONRPC2Error err = response.getError();
			
			throw new Exception("ws.getVersion failed: [" + err.getCode() + "] " + err.getMessage(), err);
		}
		
		if (! (response.getResult() instanceof String))
			throw new Exception("Unexpected ws.getVersion response, must be a string");
		
		// report version
		String wsVersion = (String)response.getResult();
		
		return new WsInfo(wsName, wsVersion);
	}
}
