package com.nimbusds.common.appendable;


/**
 * Generic appendable interface.
 */
public interface Appendable <T> {


	/**
	 * Appends the specified element.
	 *
	 * @param element The element to append. May be {@code null}.
	 */
	public void append(final T element);
}