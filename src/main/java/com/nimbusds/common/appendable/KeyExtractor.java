package com.nimbusds.common.appendable;


/**
 * Key extractor, intended for {@link JSONObjectWriter} use.
 */
public interface KeyExtractor<T> {


	/**
	 * Extracts or determines a JSON object key from the specified object.
	 *
	 * @param object The object. Must not be {@code null}.
	 *
	 * @return The JSON object key, {@code null} if extraction failed.
	 */
	public String extractKey(final T object);
}
