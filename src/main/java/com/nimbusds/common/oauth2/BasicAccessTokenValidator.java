package com.nimbusds.common.oauth2;


import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;

import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;


/**
 * Basic access token validator. Supports servlet-based and JAX-RS based web
 * applications.
 */
public class BasicAccessTokenValidator {


	/**
	 * Error response: Missing OAuth 2.0 Bearer access token.
	 */
	public static final ErrorResponse MISSING_BEARER_TOKEN;


	/**
	 * Error response: Invalid OAuth 2.0 Bearer access token.
	 */
	public static final ErrorResponse INVALID_BEARER_TOKEN;


	/**
	 * Error response: Web API disabled.
	 */
	public static final ErrorResponse WEB_API_DISABLED;


	static {
		JSONObject o = new JSONObject();
		o.put("error", "missing_token");
		o.put("error_description", "Unauthorized: Missing Bearer access token");
		MISSING_BEARER_TOKEN = new ErrorResponse(
			BearerTokenError.MISSING_TOKEN.getHTTPStatusCode(),
			BearerTokenError.MISSING_TOKEN.toWWWAuthenticateHeader(),
			o.toJSONString());

		o = new JSONObject();
		o.put("error", BearerTokenError.INVALID_TOKEN.getCode());
		o.put("error_description", "Unauthorized: Invalid Bearer access token");
		INVALID_BEARER_TOKEN = new ErrorResponse(
			BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(),
			BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(),
			o.toJSONString());

		o = new JSONObject();
		o.put("error", "web_api_disabled");
		o.put("error_description", "Forbidden: Web API disabled");
		WEB_API_DISABLED = new ErrorResponse(403, null, o.toJSONString());
	}


	/**
	 * Bearer token error response.
	 */
	public static class ErrorResponse {


		/**
		 * The HTTP status code.
		 */
		private final int statusCode;


		/**
		 * Optional HTTP response {@code WWW-Authenticate} header.
		 */
		private final String wwwAuthHeader;


		/**
		 * The HTTP body.
		 */
		private final String body;


		/**
		 * Creates a new bearer token error response.
		 *
		 * @param statusCode    The HTTP status code.
		 * @param wwwAuthHeader The HTTP response
		 *                      {@code WWW-Authenticate} header,
		 *                      {@code null} if none.
		 * @param body          The HTTP body (application/json).
		 */
		public ErrorResponse(final int statusCode,
				     final String wwwAuthHeader,
				     final String body) {

			this.statusCode = statusCode;
			this.wwwAuthHeader = wwwAuthHeader;
			this.body = body;
		}


		/**
		 * Returns a web application exception for this error response.
		 *
		 * @return The web application exception.
		 */
		public WebApplicationException toWebAppException() {

			Response.ResponseBuilder builder = Response.status(statusCode);

			if (wwwAuthHeader != null) {
				builder.header("WWW-Authenticate", wwwAuthHeader);
			}

			return new WebApplicationException(
				builder.entity(body).type(MediaType.APPLICATION_JSON).build());
		}


		/**
		 * Applies this error response to the specified HTTP servlet
		 * response.
		 *
		 * @param servletResponse The HTTP servlet response. Must not
		 *                        be {@code null}.
		 *
		 * @throws IOException If the error response couldn't be
		 *                     written.
		 */
		public void apply(final HttpServletResponse servletResponse)
			throws IOException {

			servletResponse.setStatus(statusCode);

			if (wwwAuthHeader != null) {
				servletResponse.setHeader("WWW-Authenticate", wwwAuthHeader);
			}

			if (body != null) {
				servletResponse.setContentType("application/json");
				servletResponse.getWriter().print(body);
			}
		}
	}


	/**
	 * The access token, {@code null} access to the web API is disabled.
	 */
	private final BearerAccessToken accessToken;


	/**
	 * Creates a new basic access token validator.
	 *
	 * @param accessToken The Bearer access token. If {@code null} access
	 *                    to the web API will be disabled.
	 */
	public BasicAccessTokenValidator(final BearerAccessToken accessToken) {

		this.accessToken = accessToken;
	}


	/**
	 * Returns the Bearer access token.
	 *
	 * @return The Bearer access token, {@code null} access to the web API
	 *         is disabled.
	 */
	public BearerAccessToken getAccessToken() {

		return accessToken;
	}


	/**
	 * Validates a bearer access token passed in the specified HTTP
	 * Authorization header value.
	 *
	 * @param authzHeader The HTTP Authorization header value, {@code null}
	 *                    if not specified.
	 *
	 * @throws WebApplicationException If the header value is {@code null},
	 *                                 the web API is disabled, or the
	 *                                 Bearer access token is missing or
	 *                                 invalid.
	 */
	public void validateBearerAccessToken(final String authzHeader)
		throws WebApplicationException {

		if (StringUtils.isBlank(authzHeader)) {
			throw MISSING_BEARER_TOKEN.toWebAppException();
		}

		BearerAccessToken receivedToken;

		try {
			receivedToken = BearerAccessToken.parse(authzHeader);

		} catch (ParseException e) {
			throw MISSING_BEARER_TOKEN.toWebAppException();
		}

		// Web API disabled?
		if (accessToken == null) {
			throw WEB_API_DISABLED.toWebAppException();
		}

		// Check receivedToken
		if (! receivedToken.equals(accessToken)) {
			throw INVALID_BEARER_TOKEN.toWebAppException();
		}
	}


	/**
	 * Validates a bearer access token passed in the specified HTTP servlet
	 * request.
	 *
	 * @param servletRequest  The HTTP servlet request. Must not be
	 *                        {@code null}.
	 * @param servletResponse The HTTP servlet response. Must not be
	 *                        {@code null}.
	 *
	 * @return {@code true} if the bearer access token was successfully
	 *         validated, {@code false}.
	 *
	 * @throws IOException If the response couldn't be written.
	 */
	public boolean validateBearerAccessToken(final HttpServletRequest servletRequest,
						 final HttpServletResponse servletResponse)
		throws IOException {

		String authzHeader = servletRequest.getHeader("Authorization");

		if (StringUtils.isBlank(authzHeader)) {
			MISSING_BEARER_TOKEN.apply(servletResponse);
			return false;
		}

		BearerAccessToken receivedToken;

		try {
			receivedToken = BearerAccessToken.parse(authzHeader);

		} catch (ParseException e) {
			MISSING_BEARER_TOKEN.apply(servletResponse);
			return false;
		}

		// Web API disabled?
		if (accessToken == null) {
			WEB_API_DISABLED.apply(servletResponse);
			return false;
		}

		// Check receivedToken
		if (! receivedToken.equals(accessToken)) {
			INVALID_BEARER_TOKEN.apply(servletResponse);
			return false;
		}

		return true; // success
	}
}
