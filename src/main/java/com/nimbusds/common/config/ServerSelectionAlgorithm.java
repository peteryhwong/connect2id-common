package com.nimbusds.common.config;


/**
 * Enumeration of server selection algorithms.
 */
public enum ServerSelectionAlgorithm {


	/**
	 * Fail-over selection algorithm.
	 */
	FAILOVER,
	
	
	/**
	 * Round-robin selection algorithm.
	 */
	ROUND_ROBIN
}
