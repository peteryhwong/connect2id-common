package com.nimbusds.common.config;


import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;

import com.thetransactioncompany.util.PropertyRetriever;
import com.thetransactioncompany.util.PropertyParseException;


/**
 * Directory user, with DN and password.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class DirectoryUser implements LoggableConfiguration {


	/**
	 * The distinguished name (DN) of the directory user DN. A 
	 * {@code DN.NULL_DN} value represents an anonymous user.
	 *
	 * <p>Property key: [prefix]dn
	 */
	public final DN dn;


	/**
	 * The directory user password. An empty string represents an anonymous 
	 * user.
	 *
	 * <p>Property key: [prefix]password
	 */
	public final String password;


	/**
	 * Creates a new directory user from the specified properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]dn
	 *     <li>[prefix]password
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public DirectoryUser(final String prefix, final Properties props)
		throws PropertyParseException {

		PropertyRetriever pr = new PropertyRetriever(props);
		
		String dnString = pr.getString(prefix + "dn");

		if (dnString.isEmpty()) {

			dn = DN.NULL_DN;
		}
		else {
			try {
				dn = new DN(dnString);
				
			} catch (LDAPException e) {

				throw new PropertyParseException("Invalid DN", prefix + "dn", dnString);
			}
		}

		password = pr.getString(prefix + "password");
	}


	/** 
	 * Creates a new directory user.
	 *
	 * @param dn       The distinguished name (DN) of the directory user 
	 *                 DN. A {@code DN.NULL_DN} value represents an 
	 *                 anonymous user. Must not be {@code null}.
	 * @param password The directory user password. An empty string 
	 *                 represents an anonymous user.
	 */
	public DirectoryUser(final DN dn, final String password) {

		if (dn == null)
			throw new IllegalArgumentException("The directory user DN must not be null");

		this.dn = dn;

		if (password == null)
			throw new IllegalArgumentException("The directory user password must not be null");

		this.password = password;
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger(LOG_CATEGORY);

		if (dn.equals(DN.NULL_DN))
			log.info("Directory user DN: [anonymous]");
		else
			log.info("Directory user DN: {}", dn);
	}
}
