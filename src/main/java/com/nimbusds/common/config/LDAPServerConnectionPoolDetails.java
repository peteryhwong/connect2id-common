package com.nimbusds.common.config;


import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thetransactioncompany.util.PropertyRetriever;
import com.thetransactioncompany.util.PropertyParseException;


/**
 * LDAP server connection pool details.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class LDAPServerConnectionPoolDetails extends LDAPServerDetails {


	/**
	 * The target connection pool size.
	 *
	 * <p>Property key: [prefix]connectionPoolSize
	 */  
	public final int connectionPoolSize;


	/**
	 * The default target connection pool size.
	 */
	public static final int DEFAULT_CONNECTION_POOL_SIZE = 5;


	/**
	 * The maximum length of time in milliseconds to wait for a connection 
	 * to become available when trying to obtain a connection from the 
	 * pool. A value of zero should be used to indicate that the pool 
	 * should not block at all if no connections are available and that it 
	 * should either create a new connection or throw an exception.
	 *
	 * <p>Property key: [prefix]connectionPoolMaxWaitTime
	 */
	public final int connectionPoolMaxWaitTime;


	/**
	 * The default connection pool maximum wait time, in milliseconds.
	 */
	public static final int DEFAULT_CONNECTION_POOL_MAX_WAIT_TIME = 500;


	/**
	 * The maximum time in milliseconds that a connection in this pool may
	 * be established before it should be closed and replaced with another
	 * connection. A value of zero indicates that no maximum age should be
	 * enforced.
	 *
	 * <p>Property key: [prefix]connectionMaxAge
	 */
	public final long connectionMaxAge;


	/**
	 * The default maximum connection time.
	 */
	public static final int DEFAULT_CONNECTION_MAX_AGE = 0;


	/**
	 * Creates a new LDAP server connection pool details instance from the 
	 * specified properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 * </ul>
	 *
	 * <p>Conditionally mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]selectionAlgorithm - if more than one LDAP server 
	 *         URL is specified.
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]security = STARTTLS
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectionPoolSize = 5
	 *     <li>[prefix]connectionPoolMaxWaitTime = 500
	 *     <li>[prefix]connectionMaxAge = 0
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public LDAPServerConnectionPoolDetails(final String prefix, final Properties props)
		throws PropertyParseException {

		this(prefix, props, true);
	}


	/**
	 * Creates a new LDAP server connection pool details instance from the 
	 * specified properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>none
	 * </ul>
	 *
	 * <p>Conditionally mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 *     <li>[prefix]selectionAlgorithm - if more than one LDAP server 
	 *         URL is specified.
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]security = STARTTLS
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectionPoolSize = 5
	 *     <li>[prefix]connectionPoolMaxWaitTime = 500
	 *     <li>[prefix]connectionMaxAge = 0
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public LDAPServerConnectionPoolDetails (final String prefix, final Properties props, final boolean requireURL) 
		throws PropertyParseException {

		super(prefix, props, requireURL);

		PropertyRetriever pr = new PropertyRetriever(props);

		connectionPoolSize = pr.getOptInt(prefix + "connectionPoolSize",
			DEFAULT_CONNECTION_POOL_SIZE);
	
		connectionPoolMaxWaitTime = pr.getOptInt(prefix + "connectionPoolMaxWaitTime",
			DEFAULT_CONNECTION_POOL_MAX_WAIT_TIME);

		connectionMaxAge = pr.getOptLong(prefix + "connectionMaxAge",
			DEFAULT_CONNECTION_MAX_AGE);
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		super.log();

		if (url == null)
			return;

		Logger log = LogManager.getLogger(LOG_CATEGORY);
		
		log.info("LDAP server connection pool size: {}", connectionPoolSize);
		log.info("LDAP server connection pool max wait time: {} ms", connectionPoolMaxWaitTime);
		log.info("LDAP server connection pool max age: {} ms", connectionMaxAge);
	}
}