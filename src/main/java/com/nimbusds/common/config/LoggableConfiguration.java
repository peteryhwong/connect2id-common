package com.nimbusds.common.config;


/**
 * Loggable configuration.
 */
public interface LoggableConfiguration {


	/**
	 * The preferred logging category.
	 */
	public static final String LOG_CATEGORY = "MAIN";


	/**
	 * Logs the configuration properties at INFO level.
	 */
	public void log();
}
