package com.nimbusds.common.util;


import java.util.HashMap;
import java.util.Map;


/**
 * Map utility.
 */
public class MapUtility {
	
	
	/**
	 * Returns a copy of the specified map with all string keys converted to
	 * lower case.
	 *
	 * @param map The map to process. May be {@code null}.
	 *
	 * @return A copy of the original map with all string keys converted to
	 *         lower case, {@code null} if no original map was defined.
	 */
	public static Map<String,Object> convertMapKeysToLowerCase(final Map<String,Object> map) {
		
		if (map == null)
			return null;
		
		Map<String,Object> convertedMap = new HashMap<>();
		
		for (Map.Entry<String,Object> entry: map.entrySet()) {
		
			convertedMap.put(entry.getKey().toLowerCase(), entry.getValue());
		}
		
		return convertedMap;
	}
	
	
	/**
	 * Prevents instantiation.
	 */
	private MapUtility() {}
}
