package com.nimbusds.common.store;


/**
 * Store exception.
 */
public class StoreException extends Exception {


	/**
	 * Serial version UID.
	 */
	public static final long serialVersionUID = -306675914668424166l;


	/**
	 * Creates a new store exception.
	 *
	 * @param message   The message. May be {@code null}.
	 */
	public StoreException(final String message) {
	
		this(message, null);
	}
	
	
	/**
	 * Creates a new store exception.
	 *
	 * @param message   The message. May be {@code null}.
	 * @param cause     The exception cause, {@code null} if not specified.
	 */
	public StoreException(final String message, final Throwable cause) {
	
		super(message, cause);
	}
}
