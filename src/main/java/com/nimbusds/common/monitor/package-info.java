/**
 * Common monitor classes for Dropwizard metrics and health checks.
 */
package com.nimbusds.common.monitor;