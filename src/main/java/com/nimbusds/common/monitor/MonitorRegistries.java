package com.nimbusds.common.monitor;


import java.util.Map;

import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;


/**
 * Shared monitor registers for Dropwizard metrics and health checks.
 *
 * @see com.nimbusds.common.servlet.MonitorLauncher
 */
public class MonitorRegistries {


	/**
	 * The metrics registry.
	 */
	private static MetricRegistry metricRegistry;


	/**
	 * The health checks registry.
	 */
	private static HealthCheckRegistry healthCheckRegistry;


	/**
	 * Returns the singleton shared registry for metric instances.
	 *
	 * @return The registry.
	 */
	public static MetricRegistry getMetricRegistry() {

		if (metricRegistry != null) {
			return metricRegistry;
		} else {
			metricRegistry = new MetricRegistry();
			return metricRegistry;
		}
	}


	/**
	 * Returns the singleton shared registry for health check instances.
	 *
	 * @return The registry.
	 */
	public static HealthCheckRegistry getHealthCheckRegistry() {

		if (healthCheckRegistry != null) {
			return healthCheckRegistry;
		} else {
			healthCheckRegistry = new HealthCheckRegistry();
			return healthCheckRegistry;
		}
	}


	/**
	 * Registers a metric set.
	 *
	 * @param metricSet The metric set to register. If {@code null} the
	 *                  method will return immediately.
	 */
	public static void register(final MetricSet metricSet) {

		if (metricSet == null) {
			return;
		}

		for (Map.Entry<String,Metric> entry: metricSet.getMetrics().entrySet()) {
			register(entry.getKey(), entry.getValue());
		}
	}


	/**
	 * Registers, updates or unregisters a metric.
	 *
	 * @param name   The metric name. If {@code null} the method will
	 *               return immediately.
	 * @param metric The metric, {@code null} to unregister. If a metric
	 *               with the same name exists it will be replaced.
	 */
	public static void register(final String name, final Metric metric) {

		if (name == null) {
			return;
		}

		if (metric != null) {
			if (getMetricRegistry().getNames().contains(name)) {
				// remove previously registered metric with same name
				getMetricRegistry().remove(name);
			}
			getMetricRegistry().register(name, metric);
		} else {
			getMetricRegistry().remove(name);
		}
	}


	/**
	 * Registers, updates or unregisters a health check.
	 *
	 * @param name  The health check name. If {@code null} the method will
	 *              return immediately.
	 * @param check The health check, {@code null} to unregister. If a
	 *              metric with the same name exists it will be replaced.
	 */
	public static void register(final String name, final HealthCheck check) {

		if (name == null) {
			return;
		}

		if (check != null) {
			if (getHealthCheckRegistry().getNames().contains(name)) {
				// remove previously registered check with same name
				getHealthCheckRegistry().unregister(name);
			}
			getHealthCheckRegistry().register(name, check);
		} else {
			getHealthCheckRegistry().unregister(name);
		}
	}


	/**
	 * Prevents public instantiation.
	 */
	private MonitorRegistries() { }
}
