package com.nimbusds.common.ldap;


import java.util.HashMap;
import java.util.Map;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;

import com.unboundid.ldap.sdk.LDAPConnectionPool;


/**
 * LDAP connection metrics.
 */
public class LDAPConnectionPoolMetrics implements MetricSet {


	/**
	 * The metrics map.
	 */
	private final Map<String,Metric> metricMap = new HashMap<>();


	/**
	 * Creates a new LDAP connection metrics.
	 *
	 * @param pool   The LDAP connection pool. Must not be {@code null}.
	 * @param prefix The metrics name prefix. Must not be {@code null}.                    
	 */
	public LDAPConnectionPoolMetrics(final LDAPConnectionPool pool,
					 final String prefix) {

		metricMap.put(prefix + ".maxAvailableConnections", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return pool.getConnectionPoolStatistics().getMaximumAvailableConnections();
			}
		});

		metricMap.put(prefix + ".numAvailableConnections", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return pool.getConnectionPoolStatistics().getNumAvailableConnections();
			}
		});

		metricMap.put(prefix + ".numConnectionsClosedDefunct", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumConnectionsClosedDefunct();
			}
		});

		metricMap.put(prefix + ".numConnectionsClosedExpired", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumConnectionsClosedExpired();
			}
		});

		metricMap.put(prefix + ".numConnectionsClosedUnneeded", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumConnectionsClosedUnneeded();
			}
		});

		metricMap.put(prefix + ".numFailedCheckouts", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumFailedCheckouts();
			}
		});

		metricMap.put(prefix + ".numFailedConnectionAttempts", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts();
			}
		});

		metricMap.put(prefix + ".numReleasedValid", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumReleasedValid();
			}
		});

		metricMap.put(prefix + ".numSuccessfulCheckouts", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumSuccessfulCheckouts();
			}
		});

		metricMap.put(prefix + ".numSuccessfulCheckoutsNewConnection", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsNewConnection();
			}
		});

		metricMap.put(prefix + ".numSuccessfulCheckoutsWithoutWaiting", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsWithoutWaiting();
			}
		});

		metricMap.put(prefix + ".numSuccessfulCheckoutsAfterWaiting", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsAfterWaiting();
			}
		});

		metricMap.put(prefix + ".numSuccessfulConnectionAttempts", new Gauge<Long>() {
			@Override
			public Long getValue() {
				return pool.getConnectionPoolStatistics().getNumSuccessfulConnectionAttempts();
			}
		});
	}


	@Override
	public Map<String,Metric> getMetrics() {

		return metricMap;
	}
}
