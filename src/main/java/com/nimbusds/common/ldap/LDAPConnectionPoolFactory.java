package com.nimbusds.common.ldap;


import java.security.GeneralSecurityException;
import java.security.KeyStoreException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;

import com.unboundid.ldap.sdk.BindRequest;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.ldap.sdk.PostConnectProcessor;
import com.unboundid.ldap.sdk.ServerSet;
import com.unboundid.ldap.sdk.SimpleBindRequest;
import com.unboundid.ldap.sdk.StartTLSPostConnectProcessor;
import com.unboundid.util.ssl.SSLUtil;

import com.nimbusds.common.config.CustomKeyStoreConfiguration;
import com.nimbusds.common.config.CustomTrustStoreConfiguration;
import com.nimbusds.common.config.DirectoryUser;
import com.nimbusds.common.config.LDAPServerConnectionPoolDetails;


/**
 * Factory for establishing LDAP connection pools to a directory server.
 */
public class LDAPConnectionPoolFactory {


	/**
	 * The LDAP server connect details.
	 */
	private final LDAPServerConnectionPoolDetails ldapServer;


	/**
	 * The custom trust store for LDAP over SSL/TLS.
	 */
	private final CustomTrustStoreConfiguration customTrustStore;


	/**
	 * The custom key store for LDAP over SSL/TLS.
	 */
	private final CustomKeyStoreConfiguration customKeyStore;


	/**
	 * The directory user to bind to for each connection, {@code null} if
	 * not specified (for unauthenticated connections).
	 */
	private final DirectoryUser dirUser;


	/**
	 * Creates a LDAP connection pool factory.
	 *
	 * @param ldapServer       The LDAP server connect details. Must
	 *                         specify a URL and must not be {@code null}.
	 * @param customTrustStore The custom trust store configuration. Must
	 *                         not be {@code null}.
	 * @param customKeyStore   The custom key store configuration. Must not
	 *                         be {@code null}.
	 * @param dirUser          The directory user to bind to for each
	 *                         LDAP connection. If {@code null} connections
	 *                         will be unauthenticated.
	 */
	public LDAPConnectionPoolFactory(final LDAPServerConnectionPoolDetails ldapServer,
		                         final CustomTrustStoreConfiguration customTrustStore,
		                         final CustomKeyStoreConfiguration customKeyStore,
		                         final DirectoryUser dirUser) {

		if (ldapServer == null || ldapServer.url.length == 0)
			throw new IllegalArgumentException("The LDAP server details must not be null");
		
		this.ldapServer = ldapServer;

		if (customTrustStore == null)
			throw new IllegalArgumentException("The custom trust store must not be null");

		this.customTrustStore = customTrustStore;

		if (customKeyStore == null)
			throw new IllegalArgumentException("The custom key store must not be null");

		this.customKeyStore = customKeyStore;

		this.dirUser = dirUser;
	}


	/**
	 * Creates a new LDAP connection pool. No initial connections are
	 * established, to prevent connect exceptions if the backend is 
	 * offline at the time the pool is created.
	 *
	 * @return The LDAP connection pool.
	 *
	 * @throws KeyStoreException        If the key store could not be 
	 *                                  unlocked (for SSL/StartTLS 
	 *                                  connections).
	 * @throws GeneralSecurityException On a general security exception (for
	 *                                  SSL/StartTLS connections).
	 * @throws LDAPException            If an LDAP exception is encountered.
	 */
	public LDAPConnectionPool createLDAPConnectionPool()
		throws KeyStoreException, 
		       GeneralSecurityException, 
		       LDAPException {

		SocketFactory socketFactory = null;
		PostConnectProcessor postProc = null;

		if (ldapServer.security.equals(LDAPConnectionSecurity.SSL)) {

		        SSLUtil sslUtil = LDAPConnectionFactory.initSecureConnectionContext(
				customTrustStore,
				customKeyStore,
				ldapServer.trustSelfSignedCerts);

		        socketFactory = sslUtil.createSSLSocketFactory();
		} else if (ldapServer.security.equals(LDAPConnectionSecurity.STARTTLS)) {

		        SSLUtil sslUtil = LDAPConnectionFactory.initSecureConnectionContext(
				customTrustStore,
				customKeyStore,
				ldapServer.trustSelfSignedCerts);

		        SSLContext sslContext = sslUtil.createSSLContext();
		        postProc = new StartTLSPostConnectProcessor(sslContext);
		}

		LDAPConnectionOptions opts = new LDAPConnectionOptions();
		opts.setConnectTimeoutMillis(ldapServer.connectTimeout);

		ServerSet serverSet = LDAPServerSetFactory.create(
			ldapServer.url,
			ldapServer.selectionAlgorithm,
			socketFactory,
			opts);

		// Prepare bind request?
		BindRequest bindRequest = null;

		if (dirUser != null) {

			bindRequest = new SimpleBindRequest(dirUser.dn, dirUser.password);
		}
		

		// Start with zero initial connections to prevent
		// exceptions on init if the backend is offline
		final int numInitialConnections = 0;

		LDAPConnectionPool pool = new LDAPConnectionPool(serverSet, 
		                                                 bindRequest, 
		                                                 numInitialConnections, 
		                                                 ldapServer.connectionPoolSize,
		                                                 postProc);

		// Additional configuration
		pool.setCreateIfNecessary(true);
		pool.setMaxWaitTimeMillis(ldapServer.connectionPoolMaxWaitTime);
		pool.setMaxConnectionAgeMillis(ldapServer.connectionMaxAge);

		return pool;
	}
}
