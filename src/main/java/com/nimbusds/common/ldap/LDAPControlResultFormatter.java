package com.nimbusds.common.ldap;


import java.util.LinkedHashMap;
import java.util.Map;

import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import com.unboundid.ldap.sdk.controls.VirtualListViewResponseControl;
import com.unboundid.util.Base64;


/**
 * Static method to format LDAP search control results.
 */
class LDAPControlResultFormatter {


	/**
	 * Appends any found search controls to the specified JSON object 
	 * representation of an LDAP search result.
	 *
	 * <p>The following search controls are supported:
	 *
	 * <ul><li>Simple paged results control (RFC 2696).
	 *     <li>Virtual list view control (draft-ietf-ldapext-ldapv3-vlv)
	 * </ul>
	 *
	 * @param jsonObject   The JSON object to append any included simple 
	 *                     page control and virtual list view (VLV)
	 *                     control results to. Must not be {@code null}.
	 * @param searchResult The LDAP search result. Must not be 
	 *                     {@code null}.
	 */
	public static void appendSearchControlResults(final Map<String,Object> jsonObject, 
		                                      final SearchResult searchResult) {

		// Check for search results controls
		
		for (Control c : searchResult.getResponseControls()) {
			
			if (c instanceof SimplePagedResultsControl) {
				
				SimplePagedResultsControl prc = (SimplePagedResultsControl)c;
				
				Map<String,Object> page = new LinkedHashMap<>();
				
				// the cookie is typically binary data, use BASE 64 encoding for JSON output
				page.put("totalEntryCount", prc.getSize());
				page.put("more", prc.moreResultsToReturn());

				if (prc.getCookie() != null)
					page.put("cookie", Base64.encode(prc.getCookie().getValue()));
				else
					page.put("cookie", ""); // same as last page
				
				jsonObject.put("page", page);
			}


			if (c instanceof VirtualListViewResponseControl) {

				VirtualListViewResponseControl vrc = (VirtualListViewResponseControl)c;

				Map<String,Object> vlv = new LinkedHashMap<>();

				vlv.put("totalEntryCount", vrc.getContentCount());
				vlv.put("offset", vrc.getTargetPosition());

				if (vrc.getContextID() != null)
					vlv.put("cookie", Base64.encode(vrc.getContextID().getValue()));
				else
					vlv.put("cookie", null);

				jsonObject.put("vlv", vlv);
			}
		}
	}
}