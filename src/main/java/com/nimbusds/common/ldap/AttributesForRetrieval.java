package com.nimbusds.common.ldap;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.util.NamedParamsRetriever;


/**
 * Specifies a set of directory attributes for retrieval. They are grouped 
 * into two subsets:
 *
 * <ul>
 *     <li>Attributes treated as text and encoded as UTF-8.
 *     <li>Attributes treated as binary and encoded in Base64.
 * </ul>
 */
public class AttributesForRetrieval {


	/**
	 * The retrieval specification, as required by the get entry or search 
	 * LDAP SDK API.
	 */
	private String[] spec;
	
	 
	/**
	 * The set of all selected attribute names to retrieve (UTF-8 text and 
	 * binary). The names are normalised to lower case.
	 */
	private final Set<String> allNames = new HashSet<>();
	
	
	/**
	 * The names of the binary attributes to retrieve. The names are 
	 * normalised to lower case.
	 */
	private final Set<String> binaryNames = new HashSet<>();
	
	
	/**
	 * Creates a default specification of attributes for retrieval which is
	 * retrieve all user attributes as UTF-8 text.
	 */
	public AttributesForRetrieval() {
		
		spec = null;
	}
	
	
	/**
	 * Gets the names of the attributes to retrieve as binary (Base64 
	 * encoded).
	 *
	 * @return The names (normalised to lower case) of the attributes to 
	 *         retrieve as binary, empty set if none.
	 */
	public Set<String> getBinaryAttributes() {
	
		return binaryNames;
	}
	
	
	/**
	 * Retrieves the specification of the attributes (UTF-8 text and binary)
	 * for retrieval for an LDAP get entry/search request.
	 *
	 * @return The attributes for retrieval, {@code null} for all.
	 */
	public String[] getSpec() {
	
		return spec;
	}
	
	
	/**
	 * Parses a JSON-RPC 2.0 parameter representing a set of string values.
	 * This can be a space/comma delimited JSON string, a JSON array of 
	 * strings or undefined to signify no specific parameters.
	 *
	 * @param paramName The parameter name. Must not be {@code null}.
	 * @param params    The named parameters to parse. Must not be 
	 *                  {@code null}.
	 *
	 * @return The values, empty array if none/undefined.
	 *
	 * @throws JSONRPC2Error If parsing failed (INVALID_PARAMS).
	 */
	public static String[] parseValues(final String paramName, 
		                           final NamedParamsRetriever params)
		throws JSONRPC2Error {
		
		// Return empty array if undefined
		if (! params.hasParam(paramName))
			return new String[]{};
		
		Object v = params.get(paramName);
		
		if (v instanceof String) {
			
			// Values specified in a space/comma delimited string
			String vString = ((String)v).trim();
			
			if (! vString.isEmpty())
				return vString.split("[\\s,]");
			else
				return new String[]{}; // no values
		}
		else if (v instanceof List) {
		
			// Values specified in a JSON string array
			List vList = (List)v;
			
			if (! vList.isEmpty())
				return params.getStringArray(paramName);
			else
				return new String[]{}; // no values
		}
		else {
			// Unexpected value(s)
			throw JSONRPC2Error.INVALID_PARAMS;
		}
	}
	
	
	/**
	 * Parses an "attributes" and a "binaryAttributes" specification from 
	 * the given named JSON-RPC parameters.
	 *
	 * @param params The named parameters to parse. Must not be 
	 *               {@code null}.
	 *
	 * @throws JSONRPC2Error If parsing failed (INVALID_PARAMS).
	 */
	public void parse(final NamedParamsRetriever params)
		throws JSONRPC2Error {
		
		// Initial state -> all user attributes
		spec = null;
		
		if (params.hasParam("attributes")) {
		
			String[] names = parseValues("attributes", params);
		
			for (String name: names) {
			
				if (name.equals("*+") || name.equals("+*")) {
					
					allNames.add("*");
					allNames.add("+");
				}
				else {
					// Includes '*', '+' and '1.1'
					allNames.add(name.toLowerCase());
				}
			}
		}
		
		
		if (params.hasParam("binaryAttributes")) {
		
			for (String name: parseValues("binaryAttributes", params)) {

				switch (name) {
					case "*":
						// Implies all, ignore
						break;
					case "1.1":
						// Implies none, ignore
						break;
					case "+":
						// Implies operational, ignore
						break;
					default:
						allNames.add(name.toLowerCase());
						binaryNames.add(name.toLowerCase());
						break;
				}
			}
		}
		
		// Update spec?
		
		if (allNames.size() > 0)
			spec = allNames.toArray(new String[allNames.size()]);
		
		else if (params.hasParam("attributes") && allNames.isEmpty())
			spec = new String[]{"1.1"};
	}
}
