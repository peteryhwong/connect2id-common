package com.nimbusds.common.ldap;


import javax.net.SocketFactory;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.ServerSet;

import com.nimbusds.common.config.CustomKeyStoreConfiguration;
import com.nimbusds.common.config.CustomTrustStoreConfiguration;
import com.nimbusds.common.config.LDAPServerDetails;


/**
 * Factory for establishing LDAP connections to a preset directory server.
 */
public class PresetLDAPConnectionFactory extends LDAPConnectionFactory {

	
	/**
	 * The LDAP server details.
	 */
	private final LDAPServerDetails ldapServer;
	
	
	/**
	 * The LDAP server set (single, fail-over or round-robin).
	 */
	private final ServerSet ldapServerSet;
	
	
	/**
	 * Creates a new preset LDAP connection factory.
	 *
	 * @param ldapServer       The LDAP server details. Must not be 
	 *                         {@code null}.
	 * @param customTrustStore The custom trust store configuration. Must 
	 *                         not be {@code null}.
	 * @param customKeyStore   The custom key store configuration. Must not 
	 *                         be {@code null}.
	 *
	 * @throws LDAPConnectionException If a SSL socket factory is required
	 *                                 and couldn't be established.
	 */
	public PresetLDAPConnectionFactory(final LDAPServerDetails ldapServer,
	                                   final CustomTrustStoreConfiguration customTrustStore,
	                                   final CustomKeyStoreConfiguration customKeyStore)
		throws LDAPConnectionException {
	
		super(customTrustStore, customKeyStore);

		if (ldapServer == null)
			throw new IllegalArgumentException("The LDAP server details must not be null");
			
		this.ldapServer = ldapServer;
		
		// Init LDAP server set
		SocketFactory socketFactory = getSocketFactory(ldapServer.security, 
			                                       customTrustStore,
			                                       customKeyStore,
			                                       ldapServer.trustSelfSignedCerts);
		
		LDAPConnectionOptions opts = new LDAPConnectionOptions();
		opts.setConnectTimeoutMillis(ldapServer.connectTimeout);
		
		ldapServerSet = LDAPServerSetFactory.create(ldapServer.url,
		                                            ldapServer.selectionAlgorithm,
							    socketFactory,
							    opts);
	}


	/**
	 * Gets the LDAP server details.
	 *
	 * @return The LDAP server details.
	 */
	public LDAPServerDetails getLDAPServerDetails() {

		return ldapServer;
	}
	
	
	/**
	 * Creates a new LDAP connection to the preset directory server.
	 *
	 * @return A new established unauthenticated LDAP connection ready for
	 *         use.
	 *
	 * @throws LDAPConnectionException If a new LDAP connection could not 
	 *                                 be created.
	 */
	public LDAPConnection createLDAPConnection()
		throws LDAPConnectionException {
		
		return createLDAPConnection(ldapServerSet, ldapServer.security, ldapServer.trustSelfSignedCerts);
	}
}
