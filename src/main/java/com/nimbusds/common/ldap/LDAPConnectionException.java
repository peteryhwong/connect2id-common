package com.nimbusds.common.ldap;


import java.io.IOException;

import com.unboundid.ldap.sdk.LDAPException;


/**
 * LDAP connection exception.
 */
public class LDAPConnectionException extends Exception {


	/**
	 * Enumeration of LDAP connection exception cause types.
	 */
	public static enum CauseType {

		/**
		 * General LDAP connection error.
		 */
		LDAP_CONNECT_ERROR,


		/**
		 * Unknown server host name.
		 */
		UNKNOWN_HOST,


		/**
		 * No route to server host.
		 */
		NO_ROUTE_TO_HOST,


		/**
		 * LDAP server down or incorrect port.
		 */
		LDAP_SERVER_DOWN,


		/**
		 * TCP/IP protocol exception.
		 */
		TCPIP_ERROR,


		/**
		 * TLS/SSL store error.
		 */
		KEYSTORE_ERROR,

		/**
		 * Bad server X.509 certificate.
		 */
		BAD_CERT,


		/**
		 * TLS/SSL error.
		 */
		TLS_SSL_ERROR,


		/**
		 * StartTLS error.
		 */
		STARTTLS_ERROR,


		/**
		 * LDAP connect timeout.
		 */
		CONNECT_TIMEOUT,


		/**
		 * Network I/O error.
		 */
		NETWORK_IO_ERROR
	}


	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -1421163149598083277l;


	/**
	 * The cause type.
	 */
	private final CauseType causeType;

	
	/**
	 * Creates a new LDAP connection exception, with no specific cause 
	 * type.
	 *
	 * @param message The exception message. Should not be {@code null}.
	 */
	public LDAPConnectionException(final String message) {
	
		this(message, null, null);
	}


	/**
	 * Creates a new LDAP connection exception.
	 *
	 * @param message   The exception message. Should not be {@code null}.
	 * @param causeType The cause type, {@code null} if not specified.
	 */
	public LDAPConnectionException(final String message, final CauseType causeType) {
	
		this(message, causeType, null);
	}
	
	
	/**
	 * Creates a new LDAP connection exception.
	 *
	 * @param message   The exception message. Should not be {@code null}.
	 * @param causeType The cause type, {@code null} if not specified.
	 * @param cause     The exception cause, {@code null} if not specified.
	 */
	public LDAPConnectionException(final String message, 
	                               final CauseType causeType,
		                       final Throwable cause) {
	
		super(message, cause);

		if (causeType == null)
			this.causeType = CauseType.LDAP_CONNECT_ERROR;
		else
			this.causeType = causeType;
	}


	/**
	 * Creates a new LDAP connection exception from the specified LDAP SDK
	 * exception.
	 *
	 * <p>This method contains special logic to compensate for a "bug" in
	 * the Unboundid LDAP SDK (found in 1.1.3) where network errors are 
	 * poorly reported: instead of throwing a java.net.* exception, the SDK
	 * produces a general IOException where you need to scan its message
	 * text to find out the exact network cause (see 
	 * unbounded.ldap.sdk.LDAPConnectionInternals() #line 142).
	 *
	 * @param e The LDAP SDK exception. Must not be {@code null}.
	 *
	 * @return The matching LDAP connection exception.
	 */
	public static LDAPConnectionException parse(final LDAPException e) {

		// Pinpoint error cause
		Throwable cause = e.getCause();

		if (cause == null)
			return new LDAPConnectionException("General LDAP server connect error",
				                           CauseType.LDAP_CONNECT_ERROR);

		else if (cause instanceof java.net.UnknownHostException)
			return new LDAPConnectionException("Unknown LDAP server host: " + cause.getMessage(),
				                           CauseType.UNKNOWN_HOST,
				                           cause);

		else if (cause instanceof java.net.NoRouteToHostException)
			return new LDAPConnectionException("No route to LDAP server host: " + cause.getMessage(),
				                           CauseType.NO_ROUTE_TO_HOST,
				                           cause);

		else if (cause instanceof java.net.ConnectException)
			return new LDAPConnectionException("LDAP server down or incorrect port: " + cause.getMessage(),
				                           CauseType.LDAP_SERVER_DOWN,
				                           cause);

		else if (cause instanceof java.net.ProtocolException)
			return new LDAPConnectionException("TCP/IP protocol error: " + cause.getMessage(),
				                           CauseType.TCPIP_ERROR,
				                           cause);

		else if (cause instanceof IOException) {

			// If generic IO exception, attempt to scan for cause in message
			// text (see method javadoc for SDK bug description)
			
			String msg = e.getMessage();

			if (msg.contains("java.net.UnknownHostException"))
				return new LDAPConnectionException("Unknown LDAP server host: " + cause.getMessage(),
				                                   CauseType.UNKNOWN_HOST,
				                                   cause);

			else if (msg.contains("java.net.NoRouteToHostException"))
				return new LDAPConnectionException("No route to LDAP server host: " + cause.getMessage(),
					                           CauseType.NO_ROUTE_TO_HOST,
					                           cause);

			else if (msg.contains("java.net.ConnectException"))
				return new LDAPConnectionException("LDAP server down or incorrect port: " + cause.getMessage(),
					                           CauseType.LDAP_SERVER_DOWN,
					                           cause);

			else if (msg.contains("java.net.ProtocolException"))
				return new LDAPConnectionException("TCP/IP protocol error: " + cause.getMessage(),
					                           CauseType.TCPIP_ERROR,
					                           cause);

			else if (msg.contains("sun.security.validator.ValidatorException"))
				return new LDAPConnectionException("Bad server X.509 certificate: " + cause.getMessage(),
				                           CauseType.BAD_CERT,
				                           cause);

			else if (msg.contains("javax.net.ssl"))
				return new LDAPConnectionException("TLS/SSL error: " + cause.getMessage(),
				                           CauseType.TLS_SSL_ERROR,
				                           cause);

			else if (msg.contains("timeout"))
				return new LDAPConnectionException("LDAP connect timeout: " + cause.getMessage(),
					                           CauseType.CONNECT_TIMEOUT,
					                           cause);
			else
				return new LDAPConnectionException("Network I/O error: " + cause.getMessage(),
					                           CauseType.NETWORK_IO_ERROR,
					                           cause);
		}
		else
			// Report generic connect error
			return new LDAPConnectionException("General LDAP server connect error",
				                           CauseType.LDAP_CONNECT_ERROR);
	}


	/**
	 * Gets the cause type.
	 *
	 * @return The cause type.
	 */
	public CauseType getCauseType() {

		return causeType;
	}
}
