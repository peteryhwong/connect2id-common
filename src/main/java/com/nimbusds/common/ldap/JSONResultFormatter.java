package com.nimbusds.common.ldap;


import java.util.*;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchResultReference;
import com.unboundid.ldap.sdk.schema.AttributeSyntaxDefinition;
import com.unboundid.ldap.sdk.schema.AttributeTypeDefinition;
import com.unboundid.ldap.sdk.schema.AttributeUsage;
import com.unboundid.ldap.sdk.schema.MatchingRuleDefinition;
import com.unboundid.ldap.sdk.schema.MatchingRuleUseDefinition;
import com.unboundid.ldap.sdk.schema.ObjectClassDefinition;
import com.unboundid.ldap.sdk.schema.ObjectClassType;
import com.unboundid.util.Base64;


/**
 * Static methods to format complex LDAP result structures as JSON.
 *
 * <p>See the related {@link LDIFResultFormatter} class for LDIF result
 * formatting.
 */
public class JSONResultFormatter {
	
	
	/**
	 * Formats an LDAP directory entry as a JSON object.
	 *
	 * <p>Format:
	 *
	 * <pre>
	 * {
	 *   "DN"               : "uid=user001,ou=people,dc=example,dc=com",
	 *   "attribute-name-1" : [value-1, value-2, value-3, ...],
	 *   "attribute-name-2" : [value-1, value-2, ...],
	 *   "attribute-name-3" : [value-1, ...],
	 *   ...
	 * }
	 * </pre>
	 *
	 * @param entry     The directory entry. Must not be {@code null}.
	 * @param binary    The name of the attributes to Base64 encode. Must 
	 *                  not be {@code null}.
	 * @param omitDN    If {@code true} the DN will be omitted from the 
	 *                  returned map.
	 * @param normalize If {@code true} attribute names will be converted 
	 *                  to lower case.
	 *
	 * @return A JSON object representing the directory entry.
	 */
	public static Map<String,Object> formatEntry(final Entry entry, 
		                                     final Set<String> binary, 
		                                     final boolean omitDN,
		                                     final boolean normalize) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
		
		if (! omitDN)
			jsonObject.put("DN", entry.getDN());
		
		Collection<Attribute>attributes = entry.getAttributes();
		
		for (final Attribute a : attributes) {
			
			List<String> values = new LinkedList<>();
			
			// Does the attribute require BASE-64 encoding
			if (binary.contains(a.getName().toLowerCase())) {
			
				// Apply BASE64 encoding if the attribute
				// contains at least one binary value
			
				for (final byte[] binVal: a.getValueByteArrays())
					values.add(Base64.encode(binVal));	
			}
			else {
				// Default UTF-8 encoding (from LDAP SDK)
				Collections.addAll(values, a.getValues());
			}

			String name = a.getBaseName();

			if (normalize)
				name = name.toLowerCase();
			
			jsonObject.put(name, values);
		}
		
		return jsonObject;
	}
	
	
	/**
	 * Formats an LDAP directory entry as a JSON object.
	 *
	 * <p>Format:
	 *
	 * <pre>
	 * {
	 *   "DN"               : "uid=user001,ou=people,dc=example,dc=com",
	 *   "attribute-name-1" : [value-1, value-2, value-3, ...],
	 *   "attribute-name-2" : [value-1, value-2, ...],
	 *   "attribute-name-3" : [value-1, ...],
	 *   ...
	 * }
	 * </pre>
	 *
	 * @param entry     The directory entry. Must not be {@code null}.
	 * @param binary    The name of the attributes to Base64 encode. Must 
	 *                  not be {@code null}.
	 * @param normalize If {@code true} attribute names will be converted 
	 *                  to lower case.
	 *
	 * @return A JSON object representing the directory entry.
	 */
	public static Map<String,Object> formatEntry(final Entry entry, 
		                                     final Set<String> binary,
		                                     final boolean normalize) {
	
		return formatEntry(entry, binary, false, normalize);
	}
	
	
	/**
	 * Formats a LDAP search result as a JSON object containing matches 
	 * and referrals.
	 *
	 * <p>Format:
	 *
	 * <pre>
	 * { 
	 *   "matches"    : [ { entry-1 }, { entry-2 }, { entry-3 }, ...],
	 *   "referrals"  : [ "url-1", "url-2", "url-3", ...],
	 *   "page"       : { "totalEntryCount" : n,
	 *                    "more"            : true|false,
	 *                    "cookie"          : "..." },
	 *   "vlv"        : { "totalEntryCount" : n,
	 *                    "offset"          : n,
	 *                    "cookie"          : "..." }
	 * }
	 * </pre>
	 *
	 * where {@code entry-n} are formatted by the {@link #formatEntry} 
	 * method.
	 *
	 * @param sr        The search result. Must not be {@code null}.
	 * @param binary    The name of the attributes to Base64 encode. Must 
	 *                  not be {@code null}.
	 * @param normalize If {@code true} attribute names will be converted 
	 *                  to lower case.
	 *
	 * @return A JSON object containing the search result matches, 
	 *         referrals and optional control data.
	 */
	public static Map<String,Object> formatSearchResult(final SearchResult sr, 
		                                            final Set<String> binary,
		                                            final boolean normalize) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
			
		// A result consists of possible matching entries and referrals
		
		List<Map<String,Object>> matches = new LinkedList<>();
			
		for (SearchResultEntry entry: sr.getSearchEntries())
			matches.add(JSONResultFormatter.formatEntry(entry, binary, normalize));
		
		jsonObject.put("matches", matches);
		
		List<String> referrals = new LinkedList<>();
		
		for (SearchResultReference ref: sr.getSearchReferences())
			Collections.addAll(referrals, ref.getReferralURLs());
	
		jsonObject.put("referrals", referrals);

		// Append any simple-page control or virtual-list-view control results
		LDAPControlResultFormatter.appendSearchControlResults(jsonObject, sr);
		
		return jsonObject;
	}
	
	
	/**
	 * Formats an object class defintion.
	 *
	 * @param def The object class definition. Must not be {@code null}.
	 *
	 * @return The object class properties as JSON object.
	 */
	public static Map<String,Object> formatObjectClass(final ObjectClassDefinition def) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
	
		String oid = def.getOID();
		jsonObject.put("OID", oid);
		
		List<String> names = Arrays.asList(def.getNames());
		jsonObject.put("names", names);
		
		String description = def.getDescription();
		jsonObject.put("description", description);
		
		boolean obsolete = def.isObsolete();
		jsonObject.put("obsolete", obsolete);
		
		
		ObjectClassType type = def.getObjectClassType();
		
		if (type != null)
			jsonObject.put("type", type.toString());
		else
			jsonObject.put("type", null);
		
		
		List<String> requiredAttributes = Arrays.asList(def.getRequiredAttributes());
		jsonObject.put("requiredAttributes", requiredAttributes);
		
		List<String> optionalAttributes = Arrays.asList(def.getOptionalAttributes());
		jsonObject.put("optionalAttributes", optionalAttributes);
		
		List<String> superClasses = Arrays.asList(def.getSuperiorClasses());
		jsonObject.put("superClasses", superClasses);
		
		String rawDefinition = def.toString();
		jsonObject.put("rawDefinition", rawDefinition);
	
		return jsonObject;
	}
	
	
	/**
	 * Formats an attribute type defintion.
	 *
	 * @param def The attribute type definition. Must not be {@code null}.
	 *
	 * @return The attribute type properties as JSON object.
	 */
	public static Map<String,Object> formatAttributeType(final AttributeTypeDefinition def) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
		
		// details
		
		String oid = def.getOID();
		jsonObject.put("OID", oid);
		
		List<String> names = Arrays.asList(def.getNames());
		jsonObject.put("names", names);
		
		String description = def.getDescription();
		jsonObject.put("description", description);
		
		AttributeUsage usage = def.getUsage();
		jsonObject.put("usage", usage.toString());
		
		
		// flags
		
		boolean obsolete = def.isObsolete();
		jsonObject.put("obsolete", obsolete);
		
		boolean singleValued = def.isSingleValued();
		jsonObject.put("singleValued", singleValued);
		
		boolean readOnly = def.isNoUserModification();
		jsonObject.put("readOnly", readOnly);
		
		boolean collective = def.isCollective();
		jsonObject.put("collective", collective);
	
	
		// syntax
		
		String syntaxOID = def.getSyntaxOID();
		jsonObject.put("syntaxOID", syntaxOID);
		
		
		// matching rules
		
		String equalityRule = def.getEqualityMatchingRule();
		jsonObject.put("equalityMatch", equalityRule);
		
		String orderRule = def.getOrderingMatchingRule();
		jsonObject.put("orderingMatch", orderRule);
		
		String substringRule = def.getSubstringMatchingRule();
		jsonObject.put("substringMatch", substringRule);
		
		
		// supertype
		String superType = def.getSuperiorType();
		jsonObject.put("superType", superType);
		
		
		// raw schema definition
		String rawDefinition = def.toString();
		jsonObject.put("rawDefinition", rawDefinition);
		
		return jsonObject;
	}
	
	
	/**
	 * Formats a matching rule defintion.
	 *
	 * @param def The matching rule definition. Must not be {@code null}.
	 *
	 * @return The matching rule properties as JSON object.
	 */
	public static Map<String,Object> formatMatchingRule(final MatchingRuleDefinition def) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
		
		// details
		
		String oid = def.getOID();
		jsonObject.put("OID", oid);
		
		List<String> names = Arrays.asList(def.getNames());
		jsonObject.put("names", names);
		
		String description = def.getDescription();
		jsonObject.put("description", description);
		
		
		// flags
		boolean obsolete = def.isObsolete();
		jsonObject.put("obsolete", obsolete);
	
	
		// syntax
		String syntaxOID = def.getSyntaxOID();
		jsonObject.put("syntaxOID", syntaxOID);
		
		
		// raw schema definition
		String rawDefinition = def.toString();
		jsonObject.put("rawDefinition", rawDefinition);
		
		return jsonObject;
	}
	
	
	/**
	 * Formats a matching rule use defintion.
	 *
	 * @param def The matching rule use definition. Must not be 
	 *            {@code null}.
	 *
	 * @return The matching rule use properties as JSON object.
	 */
	public static Map<String,Object> formatMatchingRuleUse(final MatchingRuleUseDefinition def) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
		
		// details
		
		String oid = def.getOID();
		jsonObject.put("OID", oid);
		
		List<String> names = Arrays.asList(def.getNames());
		jsonObject.put("names", names);
		
		String description = def.getDescription();
		jsonObject.put("description", description);
		
		
		// flags
		boolean obsolete = def.isObsolete();
		jsonObject.put("obsolete", obsolete);
	
	
		// applicable attribute types
		List<String> applicableTypes = Arrays.asList(def.getApplicableAttributeTypes());
		jsonObject.put("applicableTypes", applicableTypes);
		
		
		// raw schema definition
		String rawDefinition = def.toString();
		jsonObject.put("rawDefinition", rawDefinition);
		
		return jsonObject;
	}
	
	
	/**
	 * Formats an attribute syntax defintion.
	 *
	 * @param def The attribute syntax definition. Must not be 
	 *            {@code null}.
	 *
	 * @return The syntax properties as JSON object.
	 */
	public static Map<String,Object> formatSyntax(final AttributeSyntaxDefinition def) {
	
		Map<String,Object> jsonObject = new LinkedHashMap<>();
		
		// details
		
		String oid = def.getOID();
		jsonObject.put("OID", oid);
		
		String description = def.getDescription();
		jsonObject.put("description", description);
		
		
		// raw schema definition
		String rawDefinition = def.toString();
		jsonObject.put("rawDefinition", rawDefinition);
		
		return jsonObject;
	}


	/**
	 * Prevents instantiation.
	 */
	private JSONResultFormatter() {

		// Nothing to do
	}
}
