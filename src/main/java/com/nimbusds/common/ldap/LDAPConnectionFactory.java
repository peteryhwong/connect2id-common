package com.nimbusds.common.ldap;


import java.security.GeneralSecurityException;
import java.security.KeyStoreException;

import javax.net.SocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;

import com.unboundid.ldap.sdk.ExtendedRequest;
import com.unboundid.ldap.sdk.ExtendedResult;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.ServerSet;
import com.unboundid.ldap.sdk.SingleServerSet;
import com.unboundid.ldap.sdk.extensions.StartTLSExtendedRequest;
import com.unboundid.util.ssl.SSLUtil;
import com.unboundid.util.ssl.KeyStoreKeyManager;
import com.unboundid.util.ssl.TrustAllTrustManager;
import com.unboundid.util.ssl.TrustStoreTrustManager;

import com.nimbusds.common.config.CustomKeyStoreConfiguration;
import com.nimbusds.common.config.CustomTrustStoreConfiguration;


/**
 * Factory for establishing LDAP connections to a directory server.
 */
public class LDAPConnectionFactory {
	
	
	/**
	 * The custom trust store for LDAP over SSL/TLS.
	 */
	private final CustomTrustStoreConfiguration customTrustStore;
	
	
	/**
	 * The custom key store for LDAP over SSL/TLS.
	 */
	private final CustomKeyStoreConfiguration customKeyStore;
	
	
	
	/**
	 * Creates a new LDAP connection factory.
	 *
	 * @param customTrustStore The custom trust store configuration. Must 
	 *                         not be {@code null}.
	 * @param customKeyStore   The custom key store configuration. Must not 
	 *                         be {@code null}.
	 */
	public LDAPConnectionFactory(final CustomTrustStoreConfiguration customTrustStore,
				     final CustomKeyStoreConfiguration customKeyStore) {
	
		if (customTrustStore == null)
			throw new IllegalArgumentException("The custom TLS/SSL trust store configuration must not be null");
		
		this.customTrustStore = customTrustStore;
		
		if (customKeyStore == null)
			throw new IllegalArgumentException("The custom TLS/SSL key store configuration must not be null");
		
		this.customKeyStore = customKeyStore;
	}


	/**
	 * Gets the custom trust store configuration.
	 *
	 * @return The custom trust store configuration.
	 */
	public CustomTrustStoreConfiguration getCustomTrustStoreConfiguration() {

		return customTrustStore;
	}


	/**
	 * Gets the custom key store configuration.
	 *
	 * @return The custom key store configuration.
	 */
	public CustomKeyStoreConfiguration getCustomKeyStoreConfiguration() {

		return customKeyStore;
	}

	
	/**
	 * Initialises the context for a secure LDAP connection by creating the
	 * required TLS/SSL trust and key managers.
	 *
	 * @param customTrustStore     The custom trust store configuration. 
	 *                             Must not be {@code null}.
	 * @param customKeyStore       The custom key store configuration. Must 
	 *                             not be {@code null}.
	 * @param trustSelfSignedCerts The trust policy for self-signed X.509
	 *                             certificates presented by the LDAP
	 *                             server.
	 *
	 * @return A helper for creating the SSL context and sockets.
	 *
	 * @throws KeyStoreException On a client key store exception.
	 */
	public static SSLUtil initSecureConnectionContext(final CustomTrustStoreConfiguration customTrustStore,
		                                          final CustomKeyStoreConfiguration customKeyStore,
		                                          final boolean trustSelfSignedCerts)
		throws KeyStoreException {

		// Configure a trust manager
		TrustManager trustManager;

		if (trustSelfSignedCerts) {

		        // Accept self-signed certs presented by the server, 
		        // no checks against local trust store
		        
		        boolean examineValidityDates = true;
		        
		        trustManager = new TrustAllTrustManager(examineValidityDates);
		}
		else if (customTrustStore.enable) {

		        // Use custom trust store file provided with this
		        // web application
		        
		        boolean examineValidityDates = true;                    
		        
		        trustManager = new TrustStoreTrustManager(customTrustStore.file,
		                                                  customTrustStore.password.toCharArray(),
		                                                  customTrustStore.type,
		                                                  examineValidityDates);
		}
		else {
		        // Fall back to default system trust manager (if any)
		        trustManager = null; 
		}

		// Configure a key manager
		KeyManager keyManager;

		if (customKeyStore.enable) {
		        
		        // Use custom key store file provided with this
		        // web application
		        
		        String certificateAlias = null;
		        
		        keyManager = new KeyStoreKeyManager(customKeyStore.file,
		                                            customKeyStore.password.toCharArray(),
		                                            customKeyStore.type,
		                                            certificateAlias);
		}
		else {
		        // Fall back to default system key manager (if any)
		        keyManager = null;
		}

		return new SSLUtil(keyManager, trustManager);
	}


	/**
	 * Creates a new socket factory according to the specified LDAP
	 * connection security settings.
	 *
	 * @param security             The requested LDAP connection security.
	 *                             Must not be {@code null}.
	 * @param customTrustStore     The custom trust store configuration. 
	 *                             Must not be {@code null}.
	 * @param customKeyStore       The custom key store configuration. Must 
	 *                             not be {@code null}.
	 * @param trustSelfSignedCerts The trust policy for self-signed X.509
	 *                             certificates presented by the LDAP
	 *                             server.
	 *
	 * @return A configured SSL socket factory, {@code null} for a plain
	 *         connection.
	 *
	 * @throws LDAPConnectionException On a failure to create an SSL socket
	 *                                 factory.
	 */
	public static SocketFactory getSocketFactory(final LDAPConnectionSecurity security,
		                                     final CustomTrustStoreConfiguration customTrustStore,
		                                     final CustomKeyStoreConfiguration customKeyStore,
		                                     final boolean trustSelfSignedCerts)
		throws LDAPConnectionException {

		// Only SSL required special socket factory
		if (security != LDAPConnectionSecurity.SSL)
			return null;

		try {
			SSLUtil sslUtil = initSecureConnectionContext(customTrustStore,
				                                      customKeyStore,
				                                      trustSelfSignedCerts);
			
			return sslUtil.createSSLSocketFactory();
				
		} catch (KeyStoreException e) {
			
			throw new LDAPConnectionException("Key store exception: " + e.getMessage(),
				                          LDAPConnectionException.CauseType.KEYSTORE_ERROR,
				                          e);
		
		} catch (GeneralSecurityException e) {

			throw new LDAPConnectionException("Couldn't create SSL socket factory: " + e.getMessage(), 
				                          LDAPConnectionException.CauseType.TLS_SSL_ERROR,
				                          e);
		}
	}
	
	
	/**
	 * Applies StartTLS to the specified LDAP connection.
	 *
	 * @param con A preconfigured and established LDAP connection to have
	 *            StartTLS applied to. Must not be {@code null}.
	 *
	 * @throws LDAPConnectionException  If StartTLS couldn't be applied to
	 *                                  the LDAP connection.
	 */
	private void applyStartTLS(final LDAPConnection con, 
		                   final boolean trustSelfSignedCerts)
		throws LDAPConnectionException {
		
		// Init key store
		SSLUtil sslUtil;

		try {
			sslUtil = initSecureConnectionContext(customTrustStore,
				                              customKeyStore,
				                              trustSelfSignedCerts);

		} catch (KeyStoreException e) {

			throw new LDAPConnectionException("Key store exception: " + e.getMessage(),
				                          LDAPConnectionException.CauseType.KEYSTORE_ERROR,
				                          e);
		}
		
		// Init client-side TLS/SSL context
		SSLContext sslContext;

		try {
			sslContext = sslUtil.createSSLContext();

		} catch (GeneralSecurityException e) {

			throw new LDAPConnectionException("TLS/SSL error: " + e.getMessage(),
				                          LDAPConnectionException.CauseType.TLS_SSL_ERROR,
				                          e);
		}
		
		// Start TLS ext. op. (OID 1.3.6.1.4.1.1466.20037)
                ExtendedResult extResult;
                
                try {
                        ExtendedRequest extRequest = new StartTLSExtendedRequest(sslContext);
                        extResult = con.processExtendedOperation(extRequest);
                
                } catch (LDAPException e) {
                        
                        con.close();
                        
                        Throwable cause = e.getCause();
                        
                        if (cause != null && cause instanceof SSLHandshakeException)
                                throw new LDAPConnectionException("Bad server X.509 certificate: " + e.getMessage(),
                                	                          LDAPConnectionException.CauseType.BAD_CERT,
                                	                          e);
                        else
                                throw LDAPConnectionException.parse(e); // Reformat exception
                }
                
                if (extResult.getResultCode() != ResultCode.SUCCESS) {

                        // The StartTLS negotiation failed for some reason, 
                        // the connection can no longer be used.
                        con.close();

                        throw new LDAPConnectionException("StartTLS exception: " + extResult.getDiagnosticMessage(),
                        	                          LDAPConnectionException.CauseType.STARTTLS_ERROR,
                        	                          null);
                }
	}


	/**
	 * Creates a new LDAP connection to the specified directory server.
	 *
	 * @param host                 The LDAP server host name / IP address.
	 *                             Must not be {@code null}.
	 * @param port                 The LDAP server port.
	 * @param security             The LDAP connection security. Must not
	 *                             be {@code null}.
	 * @param timeout              The timeout in milliseconds for LDAP 
	 *                             connect requests. If zero the underlying
	 *                             LDAP client library will determine this 
	 *                             value. 
	 * @param trustSelfSignedCerts The trust policy for self-signed X.509
	 *                             certificates presented by the LDAP
	 *                             server.
	 *
	 * @return A new established unauthenticated LDAP connection ready for
	 *         use.
	 *
	 * @throws LDAPConnectionException If a new LDAP connection could not 
	 *                                 be created.
	 */
	public LDAPConnection createLDAPConnection(final String host,
		                                   final int port,
		                                   final LDAPConnectionSecurity security,
		                                   final int timeout,
		                                   final boolean trustSelfSignedCerts)
		throws LDAPConnectionException {


		// Init LDAP server set
		SocketFactory socketFactory = getSocketFactory(security, 
			                                       customTrustStore,
			                                       customKeyStore,
			                                       trustSelfSignedCerts);
		
		LDAPConnectionOptions opts = new LDAPConnectionOptions();
		opts.setConnectTimeoutMillis(timeout);
		
		ServerSet ldapServerSet = new SingleServerSet(host, port, socketFactory, opts);

		return createLDAPConnection(ldapServerSet, security, trustSelfSignedCerts);
	}


	/**
	 * Creates a new LDAP connection to the specified directory server set.
	 *
	 * @param ldapServerSet        The LDAP server set. Must not be
	 *                             {@code null}.
	 * @param security             The LDAP connection security. Must not
	 *                             be {@code null}.
	 * @param trustSelfSignedCerts The trust policy for self-signed X.509
	 *                             certificates presented by the LDAP
	 *                             server.
	 *
	 * @return A new established unauthenticated LDAP connection ready for
	 *         use.
	 *
	 * @throws LDAPConnectionException If a new LDAP connection could not 
	 *                                 be created.
	 */
	public LDAPConnection createLDAPConnection(final ServerSet ldapServerSet,
		                                   final LDAPConnectionSecurity security,
		                                   final boolean trustSelfSignedCerts)
		throws LDAPConnectionException {

		LDAPConnection con;
		
		try {
			con = ldapServerSet.getConnection();
		
		} catch (LDAPException e) {
		
			throw LDAPConnectionException.parse(e);
		}
		
		if (security != LDAPConnectionSecurity.STARTTLS)
			return con;

		// Post-process connection for Start TLS 
		applyStartTLS(con, trustSelfSignedCerts);
		
		return con;
	}
}
