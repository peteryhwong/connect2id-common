package com.nimbusds.common.ldap;


import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.OperationType;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import com.nimbusds.common.servlet.ResourceRetriever;


/**
 * Example in-memory LDAP directory server for demonstration and testing 
 * purposes. Access is limited to read and bind (authenticate) only.
 *
 * <p>The directory server is configured by a set of "exampleDirectoryServer.*"
 * properties, see {@link Configuration}.
 *
 * <p>The example directory implements {@code ServletContextListener}. This 
 * enables its automatic startup and shutdown in a servlet container (Java web 
 * server), such as Apache Tomcat. When started from a servlet container the
 * directory configuration is obtained from a properties file specified by a
 * context parameter named {@code exampleDirectoryServer.configurationFile}.
 */
public class ExampleDirectory implements ServletContextListener {
	
	
	/**
	 * The example directory server configuration.
	 */
	public static class Configuration {
	
		
		/**
		 * If {@code true} the example directory server must be 
		 * enabled.
		 *
		 * <p>Property key: exampleDirectoryServer.enable
		 */
		public final boolean enable;
		
		
		/**
		 * The default enable policy.
		 */
		public static final boolean DEFAULT_ENABLE = false;
		
		
		/**
		 * The port number on which the example directory server 
		 * must	accept LDAP client connections.
		 *
		 * <p>Property key: exampleDirectoryServer.port
		 */
		public final int port;
		
		
		/**
		 * The default port number.
		 */
		public static final int DEFAULT_PORT = 10389;
		
		
		/**
		 * Specifies an alternative schema for the example directory +
		 * server, supplied in a single LDIF file. If {@code null} the 
		 * default built-in server schema must be used.
		 *
		 * <p>Property key: exampleDirectoryServer.schema
		 */
		public final String schema;
		
		
		/**
		 * The base distinguished name (DN) of the directory information
		 * tree.
		 *
		 * <p>Property key: exampleDirectoryServer.baseDN
		 */
		public final String baseDN;
		
		
		/**
		 * The initial directory information tree, supplied in a single
		 * LDIF file. If {@code null} the directory will be left
		 * empty.
		 *
		 * <p>Property key: exampleDirectoryServer.content
		 */
		public final String content;
		
		
		/**
		 * Creates a new example directory server configuration from the
		 * specified properties.
		 *
		 * @param props The configuration properties. Must not be 
		 *              {@code null}.
		 *
		 * @throws PropertyParseException On a missing or invalid 
		 *                                property.
		 */
		public Configuration(final Properties props)
			throws PropertyParseException {
		
			PropertyRetriever pr = new PropertyRetriever(props);
			
			enable = pr.getOptBoolean("exampleDirectoryServer.enable", DEFAULT_ENABLE);
			
			if (! enable) {
				
				port = DEFAULT_PORT;
				schema = null;
				baseDN = null;
				content = null;
				return;
			}
			
			// We're okay to read rest of config
			
			port = pr.getOptInt("exampleDirectoryServer.port", DEFAULT_PORT);
			
			String s = pr.getOptString("exampleDirectoryServer.schema", null);
			
			if (s == null || s.isEmpty())
				schema = null;
			else
				schema = s;
				
			baseDN = pr.getString("exampleDirectoryServer.baseDN");
			
			s = pr.getOptString("exampleDirectoryServer.content", null);
			
			if (s == null || s.isEmpty())
				content = null;
			else
				content = s;
		}
	}
	
	
	/**
	 * The example in-memory directory server.
	 */
	private InMemoryDirectoryServer ds = null;
	
	
	/**
	 * The servlet context.
	 */
	private ServletContext servletContext;
	
	
	/** 
	 * The logger. 
	 */
	private final Logger log = LogManager.getLogger("MAIN");
	
	
	/**
	 * If the specified filename is relative returns its full path, else the
	 * filename is left unchanged.
	 *
	 * <p>Uses the servlet context {@code getRealPath} method.
	 *
	 * @param filename The filename. Must not be {@code null}.
	 *
	 * @return The full path filename.
	 */
	private String getFullPath(final String filename) {
		
		File file = new File(filename);
		
		if (file.isAbsolute())
			return filename;
		else
			return servletContext.getRealPath(File.separator) + filename;
	}
	
	
	/**
	 * Starts the example in-memory directory server. 
	 *
	 * @param config The example directory server configuration. Must not
	 *               be {@code null}.
	 *
	 * @throws LDAPException If the in-memory directory server couldn't be
	 *                       started or its initialisation failed.
	 * @throws IOException   If a schema file was specified and it couldn't
	 *                       be read.
	 * @throws LDIFException If a schema file was specified that is not 
	 *                       valid LDIF.
	 */
	public void start(final Configuration config)
		throws LDAPException,
		       IOException,
		       LDIFException {
		
		if (! config.enable) {
		
			log.info("Example directory server: disabled");
			return;
		}
		
		InMemoryListenerConfig listenerConfig = 
			InMemoryListenerConfig.createLDAPConfig("example-ds", config.port);

		// Get alternative schema, if any

		Schema schema = null;

		if (config.schema != null) {

			String schemaFullPath = getFullPath(config.schema);
			schema = Schema.getSchema(schemaFullPath);
			
			log.info("Example directory server: Schema LDIF file: {}", schemaFullPath);
		}


		InMemoryDirectoryServerConfig dsConfig = new InMemoryDirectoryServerConfig(config.baseDN);

		log.info("Example directory server: Base DN: {}", config.baseDN);

		dsConfig.setSchema(schema);
		dsConfig.setListenerConfigs(listenerConfig);

		// Limit access to read and bind only
		dsConfig.setAllowedOperationTypes(OperationType.BIND,
			                          OperationType.COMPARE,
						  OperationType.SEARCH,
						  OperationType.EXTENDED);

		// Start server
		ds = new InMemoryDirectoryServer(dsConfig);


		// Populate directory with LDIF, if any

		if (config.content != null) {

			String contentFullPath = getFullPath(config.content);
			ds.importFromLDIF(true, contentFullPath);

			log.info("Example directory server: Populated from LDIF file {}", contentFullPath);
		}

		// Start listening on selected port
		ds.startListening();

		log.info("Example directory server: Started on port {}", ds.getListenPort());
	}
	
	
	/**
	 * Stops the example in-memory directory server (if previously started).
	 * Information and status messages are logged at INFO level.
	 */
	public void stop() {
	
		if (ds == null)
			return;
		
		// Clean all connections and stop server
		ds.shutDown(true);
		
		log.info("Example directory server: Shut down");
	}
	
	
	/**
	 * Handler for servlet context startup events. Launches the example 
	 * in-memory directory server (if enabled per configuration). Exceptions
	 * are logged at ERROR level, information and status messages at INFO
	 * level.
	 *
	 * <p>The example directory server configuration is retrieved from a 
	 * properties file which location is specified by a servlet context
	 * parameter named {@code exampleDirectory.configurationFile}.
	 *
	 * @param sce A servlet context event.
	 */
	public void contextInitialized(ServletContextEvent sce) {

		servletContext = sce.getServletContext();
		
		// Read configuration
		Configuration config;

		try {
			Properties props = ResourceRetriever.getProperties(servletContext,
                                                                           "exampleDirectory.configurationFile",
                                                                           log);
		
			config = new Configuration(props);

		} catch (Exception e) {
		
			log.error("Couldn't configure example directory server: {}", e.getMessage());
			return;
		}
		
		
		// Start server
		try {
			start(config);
			
		} catch (LDAPException e) {
		
			log.error("Couldn't start example directory server: {}", e.getMessage());
			
		} catch (IOException | LDIFException e) {

			log.error("Couldn't read schema file: {}", e.getMessage());

		}
	}


	/**
	 * Handler for servlet context shutdown events. Stops the example 
	 * in-memory directory server (if previously started).
	 *
	 * @param sce A servlet context event.
	 */
	public void contextDestroyed(ServletContextEvent sce) {

		stop();
	}
}

