package com.nimbusds.common.servlet;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.Logger;


/**
 * Servlet resource retriever. Can be used to retrieve the content of a file as
 * {@code java.io.InputStream}, {@code String} or {@code java.util.Properties}.
 */
public class ResourceRetriever {


	/**
	 * Gets the specified servlet context {@code web.xml} init parameter.
	 *
	 * @param servletCtx The servlet context. Must not be {@code null}.
	 * @param paramName  The servlet context init parameter name. Must not
	 *                   {@code null}.
	 * @param logger     To log exceptions at ERROR level, {@code null} if
	 *                   none.
	 *
	 * @return The servlet context init parameter.
	 *
	 * @throws Exception If the parameter is not specified or empty.
	 */
	private static String getInitParam(final ServletContext servletCtx,
					   final String paramName,
					   final Logger logger)
		throws Exception {

		String param = servletCtx.getInitParameter(paramName);

		if (param == null || param.trim().isEmpty()) {

			final String msg = "Missing servlet context (web.xml) init parameter: " + paramName;

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg);
		}

		return param;
	}


	/**
	 * Gets the specified servlet {@code web.xml} init parameter.
	 *
	 * @param servletConfig The servlet configuration. Must not be
	 *                      {@code null}.
	 * @param paramName     The servlet init parameter name. Must not
	 *                      {@code null}.
	 * @param logger        To log exceptions at ERROR level, {@code null}
	 *                      if none.
	 *
	 * @return The servlet init parameter.
	 *
	 * @throws Exception If the parameter is not specified or empty.
	 */
	private static String getInitParam(final ServletConfig servletConfig,
					   final String paramName,
					   final Logger logger)
		throws Exception {

		String param = servletConfig.getInitParameter(paramName);

		if (param == null || param.trim().isEmpty()) {

			final String msg = "Missing servlet (web.xml) init parameter: " + paramName;

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg);
		}

		return param;
	}

	
	/**
	 * Gets the resource in the specified servlet context {@code web.xml}
	 * init parameter location.
	 *
	 * @param servletCtx  The servlet context. Must not be {@code null}.
	 * @param paramName   The name of the servlet context init parameter
	 *                    specifying the resource file (relative to the web
	 *                    app root). For example,
	 *                    {@code /WEB-INF/authService.properties}. Must not 
	 *                    be {@code null}.
	 * @param logger      To log exceptions at ERROR level, {@code null} if
	 *                    none.
	 *
	 * @return The resource as an input stream.
         *
         * @throws Exception On a missing servlet context parameter or a 
         *                   missing resource.
	 */
	public static InputStream getStream(final ServletContext servletCtx,
		                            final String paramName,
		                            final Logger logger)
		throws Exception {

		String file = getInitParam(servletCtx, paramName, logger);

		InputStream is = servletCtx.getResourceAsStream(file);

		if (is == null) {
			final String msg = "Missing file or invalid path: " + file;

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg);
		}

		return is;
	}


	/**
	 * Gets the resource in the specified servlet {@code web.xml} init
	 * parameter location.
	 *
	 * @param servletConfig The servlet configuration. Must not be
	 *                      {@code null}.
	 * @param paramName     The name of the servlet init parameter
	 *                      specifying the resource file (relative to the
	 *                      web app root). For example,
	 *                      {@code /WEB-INF/authService.properties}. Must
	 *                      not be {@code null}.
	 * @param logger        To log exceptions at ERROR level, {@code null}
	 *                      if none.
	 *
	 * @return The resource as an input stream.
	 *
	 * @throws Exception On a missing servlet init parameter or a
	 *                   missing resource.
	 */
	public static InputStream getStream(final ServletConfig servletConfig,
		                            final String paramName,
		                            final Logger logger)
		throws Exception {

		String file = getInitParam(servletConfig, paramName, logger);

		InputStream is = servletConfig.getServletContext().getResourceAsStream(file);

		if (is == null) {
			final String msg = "Missing file or invalid path: " + file;

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg);
		}

		return is;
	}


	/**
	 * Gets the resource in the specified servlet context {@code web.xml}
	 * init parameter location.
	 *
	 * @param servletCtx  The servlet context. Must not be {@code null}.
	 * @param paramName   The name of the servlet context init parameter
	 *                    specifying the resource file (relative to the web
	 *                    app root). For example,
	 *                    {@code /WEB-INF/authService.properties}. Must not
	 *                    be {@code null}.
	 * @param logger      To log exceptions at ERROR level, {@code null} if
	 *                    none.
	 *
	 * @return The resource as a string.
	 *
	 * @throws Exception On a missing servlet context parameter or a
	 *                   missing resource.
	 */
	public static String getString(final ServletContext servletCtx,
		                       final String paramName,
		                       final Logger logger)
		throws Exception {

		InputStream is = getStream(servletCtx, paramName, logger);

		StringBuilder sb = new StringBuilder();

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));

			String line;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}

			reader.close();

		} catch (IOException e) {
			final String msg = "Couldn't read file: " + e.getMessage();

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg, e);
		}

		return sb.toString();
	}


	/**
         * Gets the {@code java.util.Properties} in the specified servlet
	 * context {@code web.xml} init parameter location.
         *
	 * @param servletCtx  The servlet context. Must not be {@code null}.
	 * @param paramName   The name of the servlet context init parameter
	 *                    specifying the properties file (relative to the
	 *                    web app root). For example,
	 *                    {@code /WEB-INF/authService.properties}. Must not
	 *                    be {@code null}.
	 * @param logger      To log exceptions at ERROR level, {@code null} if
	 *                    none.
	 *
	 * @return The properties.
         *
         * @throws Exception On a missing servlet context parameter, missing or 
	 *                   bad properties file.
         */
	public static Properties getProperties(final ServletContext servletCtx,
					       final String paramName,
					       final Logger logger)
		throws Exception {
		
		String file = servletCtx.getInitParameter(paramName);

		InputStream is = getStream(servletCtx, paramName, logger);
		
                Properties props = new Properties();
		
		// Read the properties
		try {
			props.load(is);
			
		} catch (IOException e) {
			final String msg = "Couldn't read properties file: " + e.getMessage();

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg, e);
			
		} catch (IllegalArgumentException e) {
			final String msg = "Bad properties file: " + e.getMessage();

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg, e);
		}

		if (logger != null) {
			logger.info("Loaded properties file {}", file);
		}
		
		return props;
	}


	/**
	 * Gets the {@code java.util.Properties} in the specified servlet
	 * {@code web.xml} init parameter location.
	 *
	 * @param servletConfig The servlet configuration. Must not be
	 *                       {@code null}.
	 * @param paramName      The name of the servlet init parameter
	 *                       specifying the properties file (relative to
	 *                       the web app root). For example,
	 *                       {@code /WEB-INF/authService.properties}. Must
	 *                       not be {@code null}.
	 * @param logger         To log exceptions at ERROR level, {@code null}
	 *                       if none.
	 *
	 * @return The properties.
	 *
	 * @throws Exception On a missing servlet context parameter, missing or
	 *                   bad properties file.
	 */
	public static Properties getProperties(final ServletConfig servletConfig,
					       final String paramName,
					       final Logger logger)
		throws Exception {

		String file = servletConfig.getInitParameter(paramName);

		InputStream is = getStream(servletConfig, paramName, logger);

		Properties props = new Properties();

		// Read the properties
		try {
			props.load(is);

		} catch (IOException e) {
			final String msg = "Couldn't read properties file: " + e.getMessage();

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg, e);

		} catch (IllegalArgumentException e) {
			final String msg = "Bad properties file: " + e.getMessage();

			if (logger != null)
				logger.error(msg);

			throw new Exception(msg, e);
		}

		if (logger != null) {
			logger.info("Loaded properties file {}", file);
		}

		return props;
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private ResourceRetriever() { }
}
