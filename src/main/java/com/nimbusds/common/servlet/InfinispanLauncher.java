package com.nimbusds.common.servlet;


import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang3.StringUtils;

import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * Configures and launches an Infinispan cache manager at servlet context
 * startup. The cache manager is stopped at servlet context shutdown (if
 * previously launched).
 *
 * <p>The name / path of the Infinispan configuration file is specified in a
 * servlet context init parameter {@code infinispan.configurationFile}.
 *
 * <p>The launched Infinispan cache manager will be exported to the servlet 
 * context as an {@code org.infinispan.manager.EmbeddedCacheManager}, under a 
 * key that bears the interface name.
 */
public class InfinispanLauncher implements ServletContextListener {


	/**
	 * The name of the Infinispan configuration filename.
	 */
	public static final String INFINISPAN_CONFIG_FILENAME = "infinispan.configurationFile";


	/**
	 * The name of the servlet context attribute for the launched
	 * Infinispan cache manager.
	 */
	public static final String INFINISPAN_CTX_ATTRIBUTE_NAME = "org.infinispan.manager.EmbeddedCacheManager";
	
	
	/**
	 * The Infinispan cache manager.
	 */
	private EmbeddedCacheManager cacheManager;


	/**
	 * Handler for servlet context startup events; configures and launches
	 * an Infinispan cache manager using the configuration file specified 
	 * in the servlet context parameter {@code infinispan.configurationFile}. 
	 *
	 * <p>The configuration file location must be relative to the web 
	 * application directory, e.g. {@code /WEB-INF/infinispan.xml}.
	 *
	 * <p>Exceptions are logged at ERROR level using Log4j.
	 *
	 * @param sce A servlet context event.
	 */
	@Override
	public void contextInitialized(final ServletContextEvent sce) {

		Logger log = LogManager.getLogger("MAIN");

		ServletContext servletContext = sce.getServletContext();

		String configFile = servletContext.getInitParameter(INFINISPAN_CONFIG_FILENAME);
		
		if (StringUtils.isBlank(configFile)) {

			String msg = "Couldn't load Infinispan configuration: Missing servlet context parameter \"" + INFINISPAN_CONFIG_FILENAME + "\"";
			log.error(msg);
			throw new RuntimeException(msg);
		}
		
		InputStream is = servletContext.getResourceAsStream(configFile);
		
		if (is == null) {
			String msg = "Couldn't load Infinispan configuration file: " + configFile;
			log.error(msg);
			throw new RuntimeException(msg);
		}
		
		try {
			cacheManager = new DefaultCacheManager(is);

		} catch (Exception e) {

			String msg = "Couldn't start Infinispan cache manager: " + e.getMessage();
			log.error(msg, e);
			throw new RuntimeException(msg, e);
		}

		servletContext.setAttribute(INFINISPAN_CTX_ATTRIBUTE_NAME, cacheManager);
	}


	/**
	 * Handler for servlet context shutdown events. Stops the Infinispan
	 * cache manager.
	 *
	 * @param sce A servlet context event.
	 */
	@Override
	public void contextDestroyed(final ServletContextEvent sce) {

		if (cacheManager == null)
			return;

		cacheManager.stop();
	}
}
