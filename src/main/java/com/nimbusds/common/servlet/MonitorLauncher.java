package com.nimbusds.common.servlet;


import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.codahale.metrics.JmxReporter;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import com.nimbusds.common.monitor.MonitorRegistries;


/**
 * Monitor launcher. Exports the shared
 * {@link com.nimbusds.common.monitor.MonitorRegistries} into the servlet
 * context. Also enables JMX reporting if {@link #ENABLE_JMX_PROPERTY_NAME
 * configured}.
 */
public class MonitorLauncher implements ServletContextListener {


	/**
	 * The name of the servlet context parameter that specifies the
	 * configuration file location.
	 */
	public static final String CONFIG_CTX_PARAMETER_NAME = "monitor.configurationFile";


	/**
	 * The property name for the enable JMX switch.
	 */
	public static final String ENABLE_JMX_PROPERTY_NAME = "monitor.enableJMX";


	/**
	 * The JMX reporter.
	 */
	protected JmxReporter jmxReporter;


	@Override
	public void contextInitialized(final ServletContextEvent sce) {

		sce.getServletContext().setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		sce.getServletContext().setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		Logger log = LogManager.getLogger("MAIN");

		Properties props;

		try {
			props = ResourceRetriever.getProperties(
				sce.getServletContext(),
				CONFIG_CTX_PARAMETER_NAME,
				log);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}

		if (System.getProperties().containsKey(ENABLE_JMX_PROPERTY_NAME)) {
			// Override with system property
			props.setProperty(ENABLE_JMX_PROPERTY_NAME, System.getProperty(ENABLE_JMX_PROPERTY_NAME));
		}


		PropertyRetriever pr = new PropertyRetriever(props);

		boolean enableJMX;

		try {
			enableJMX = pr.getBoolean(ENABLE_JMX_PROPERTY_NAME);

		} catch (PropertyParseException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}

		if (enableJMX) {
			jmxReporter = JmxReporter.forRegistry(MonitorRegistries.getMetricRegistry()).build();
			jmxReporter.start();
			log.info("Started monitor JMX reporter");
		} else {
			log.info("Metrics JMX reported disabled");
		}
	}


	@Override
	public void contextDestroyed(final ServletContextEvent sce) {

		if (jmxReporter != null) {
			jmxReporter.stop();
		}
	}
}
