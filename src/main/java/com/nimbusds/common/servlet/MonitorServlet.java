package com.nimbusds.common.servlet;


import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import com.nimbusds.common.oauth2.BasicAccessTokenValidator;

import com.nimbusds.oauth2.sdk.token.BearerAccessToken;

import com.codahale.metrics.servlets.AdminServlet;


/**
 * Monitor servlet for exposing Dropwizard metrics and health checks, requires
 * an OAuth 2.0 bearer token for access.
 *
 * <p>The token is defined in a {@link #API_TOKEN_PROPERTY_NAME Java property}
 * obtained from 1) system properties or from 2) a properties file specified by
 * {@link com.nimbusds.common.servlet.MonitorLauncher#CONFIG_CTX_PARAMETER_NAME
 * servlet context parameter}.
 */
public class MonitorServlet extends AdminServlet {


	/**
	 * The property name for the API access token.
	 */
	public static final String API_TOKEN_PROPERTY_NAME = "monitor.apiAccessToken";


	/**
	 * The min accepted web access token character length.
	 */
	public static final int MIN_TOKEN_LENGTH = 32;


	/**
	 * The access token validator.
	 */
	protected BasicAccessTokenValidator tokenValidator;


	@Override
	public void init(final ServletConfig config)
		throws ServletException {

		super.init(config);

		Logger log = LogManager.getLogger("MAIN");

		Properties props;

		try {
			props = ResourceRetriever.getProperties(
				config.getServletContext(),
				MonitorLauncher.CONFIG_CTX_PARAMETER_NAME,
				log);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ServletException(e.getMessage(), e);
		}


		if (System.getProperties().containsKey(API_TOKEN_PROPERTY_NAME)) {
			// Override with system property
			props.setProperty(API_TOKEN_PROPERTY_NAME, System.getProperty(API_TOKEN_PROPERTY_NAME));
		}


		PropertyRetriever pr = new PropertyRetriever(props);

		String tokenString;

		try {
			tokenString = pr.getString(API_TOKEN_PROPERTY_NAME);

		} catch (PropertyParseException e) {
			log.error(e.getMessage(), e);
			throw new ServletException(e.getMessage(), e);
		}

		if (tokenString.length() < MIN_TOKEN_LENGTH) {
			String msg = "The monitor web API access token must be at least " + MIN_TOKEN_LENGTH + " characters long";
			log.error(msg);
			throw new ServletException(msg);
		}

		tokenValidator = new BasicAccessTokenValidator(new BearerAccessToken(tokenString));
	}


	@Override
	protected void doGet(final HttpServletRequest req,
			     final HttpServletResponse resp)
		throws ServletException, IOException {

		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token
		}

		super.doGet(req, resp);
	}


	@Override
	protected void service(final HttpServletRequest req,
			       final HttpServletResponse resp)
		throws ServletException, IOException {

		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token
		}

		super.service(req, resp);
	}
}
