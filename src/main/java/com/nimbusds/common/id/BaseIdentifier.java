package com.nimbusds.common.id;


import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;

import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;


/**
 * The base class for identifiers (IDs) used in Connect2id software.
 */
public abstract class BaseIdentifier implements Identifier, Comparable<Identifier>, JSONAware {
	
	
	/**
	 * The secure random generator.
	 */
	private static final SecureRandom secureRandom = new SecureRandom();
	
	
	/**
	 * The default byte length of generated identifiers.
	 */
	public static final int DEFAULT_BYTE_LENGTH = 32;

	
	/**
	 * The identifier value. 
	 */
	private final String value;
	
	
	/**
	 * Creates a new unique identifier (ID) based on a secure randomly 
	 * generated 256-bit number, Base64URL-encoded.
	 */
	public BaseIdentifier() {
	
		byte[] n = new byte[DEFAULT_BYTE_LENGTH];
		secureRandom.nextBytes(n);
		value = Base64.encodeBase64URLSafeString(n);
	}
	
	
	/**
	 * Creates a new identifier (ID) from the specified string. The value 
	 * is not validated (for legality or uniqueness) in any way.
	 *
	 * @param value The identifier (ID) value. Must not be {@code null}.
	 */
	public BaseIdentifier(final String value) {
	
		if (value == null)
			throw new IllegalArgumentException("The value must not be null");
	
		this.value = value;
	}


	@Override
	public int compareTo(final Identifier other) {

		return value.compareTo(other.toString());
	}
	
	
	/**
	 * Overrides {@code Object.hashCode()}.
	 *
	 * @return The object hash code.
	 */
	@Override
	public int hashCode() {
	
		return value.hashCode();
	}
	
	
	/**
	 * Overrides {@code Object.equals()}.
	 *
	 * @param object The object to compare to.
	 *
	 * @return {@code true} if the objects have the same value, otherwise
	 *         {@code false}.
	 */
	@Override
	public abstract boolean equals(final Object object);
	
	
	/**
	 * Returns the string representation of this identifier (ID).
	 *
	 * @return The string representation.
	 */
	@Override
	public String toString() {
	
		return value;
	}
	
	
	/**
	 * Returns the JSON string representation of this identifier (ID).
	 * 
	 * @return The JSON string representation.
	 */
	@Override
	public String toJSONString() {
	
		StringBuilder sb = new StringBuilder();
		sb.append('"');
		sb.append(JSONObject.escape(value));
		sb.append('"');
		return sb.toString();
	}
}
