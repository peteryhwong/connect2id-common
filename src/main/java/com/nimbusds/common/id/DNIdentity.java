package com.nimbusds.common.id;


import com.unboundid.ldap.sdk.DN;


/**
 * Represents a distinguished name (DN) identity.
 */
public final class DNIdentity extends BaseIdentifier {

	
	/**
	 * The DN value.
	 */
	private final DN dn;
	
	
	/**
	 * Creates a new distinguished name (DN) identity.
	 *
	 * @param dn The DN, must not be {@code null}.
	 */
	public DNIdentity(final DN dn) {
	
		super(dn.toString());
	
		this.dn = dn;
	}
	
	
	/**
	 * Gets the distinguished name (DN) value.
	 *
	 * @return The DN.
	 */
	public DN getDN() {
	
		return dn;
	}
	
	
	/**
	 * Overrides {@code Object.hashCode()}.
	 *
	 * @return The object hash code.
	 */
	@Override
	public int hashCode() {
	
		return dn.hashCode();
	}
	
	
	@Override
	public boolean equals(final Object object) {

	        return object instanceof DNIdentity && this.toString().equals(object.toString());
	}
}
