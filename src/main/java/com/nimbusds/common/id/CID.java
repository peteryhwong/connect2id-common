package com.nimbusds.common.id;


/**
 * Represents a secure immutable connection identifier (CID). Originally 
 * intended to be used as handler for LDAP connections.
 */
public final class CID extends BaseIdentifier {
	
	
	/**
	 * Creates a new unique connection identifier (CID) based on a secure 
	 * randomly generated 256-bit number, Base64URL-encoded.
	 */
	public CID() {
	
		super();
	}
	
	
	/**
	 * Creates a new connection identifier (CID) from the specified string.
	 *
	 * @param value The connection identifier (CID) value.
	 */
	public CID(final String value) {
	
		super(value);
	}
	
	
	@Override
	public boolean equals(final Object object) {

	        return object instanceof CID && this.toString().equals(object.toString());
	}
}
