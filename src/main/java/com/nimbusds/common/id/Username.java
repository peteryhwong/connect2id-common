package com.nimbusds.common.id;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Represents a username.
 *
 * <p>The {@link #isLegal} method is intended for basic validation before 
 * committing to expensive operations such as authentication. It checks whether
 * the specified string falls outside the typical scope of legal username 
 * characters and length.
 */
public final class Username extends BaseIdentifier {

	
	/**
	 * The regular expression string used to match legal usernames.
	 */
	public static final String LEGAL_USERNAME_RE = "[\\w@\\.-]{2,32}";
	
	
	/**
	 * The compiled regular expression to match legal usernames.
	 */
	private static final Pattern USERNAME_PATTERN = Pattern.compile(LEGAL_USERNAME_RE);
	
	
	/**
	 * Checks whether the specified username value is legal. This is done by
	 * performing a regular expression match against 
	 * {@link #LEGAL_USERNAME_RE}.
	 *
	 * @param value The username value to test. It is trimmed before 
	 *              testing. Must not be {@code null}.
	 *
	 * @return {@code true} if the username is legal, else {@code false}.
	 */
	public static boolean isLegal(final String value) {
	
		return isLegal(value, USERNAME_PATTERN);
	}
	
	
	/**
	 * Checks whether the specified username value is legal. This is done
	 * by performing a regular expression match against the specified
	 * regular expression.
	 *
	 * @param value The username value to test. It is trimmed before 
	 *              testing. Must not be {@code null}.
	 * @param re    The regular expression for validation, must evaluate to
	 *              {@code true} for a legal username value.
	 *
	 * @return {@code true} if the username is legal, else {@code false}.
	 */
	public static boolean isLegal(final String value, final Pattern re) {
	
		Matcher m = re.matcher(value.trim());
		return m.matches();
	}
	
	
	/**
	 * Creates a new username.
	 *
	 * @param value The username value, must not be {@code null}. The value 
	 *              is trimmed (of whitespace) internally.
	 */
	public Username(final String value) {
		
		super(value.trim());
	}
	
	
	@Override
	public boolean equals(final Object object) {

	        return object instanceof Username && this.toString().equals(object.toString());
	}
}
