package com.nimbusds.common.id;


import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;

import org.infinispan.commons.marshall.AdvancedExternalizer;


/**
 * Identifier externalisers, required by Infinispan marshalling.
 */
public class IdentifierExternalizer implements AdvancedExternalizer<BaseIdentifier> {


	@Override
	public Set<Class<? extends BaseIdentifier>> getTypeClasses() {

		Set<Class<? extends BaseIdentifier>> types = new HashSet<>();

		types.add(SID.class);
		types.add(UID.class);
		types.add(Username.class);
		types.add(CID.class);
		types.add(AuthzId.class);
		types.add(DNIdentity.class);

		return types;
	}


	@Override
	public Integer getId() {

		return 10000;
	}


	@Override
	public void writeObject(final ObjectOutput objectOutput, final BaseIdentifier identifier)
		throws IOException {

		objectOutput.writeUTF(identifier.getClass().getName());
		objectOutput.writeUTF(identifier.toString());
	}


	@Override
	public BaseIdentifier readObject(final ObjectInput objectInput)
		throws IOException, ClassNotFoundException {

		String className = objectInput.readUTF();
		String value = objectInput.readUTF();

		if (className.equals(SID.class.getName())) {
			return new SID(value);
		} else if (className.equals(UID.class.getName())) {
			return new UID(value);
		} else if (className.equals(Username.class.getName())) {
			return new Username(value);
		} else if (className.equals(CID.class.getName())) {
			return new CID(value);
		} else if (className.equals(AuthzId.class.getName())) {

			try {
				return AuthzId.parse(value);
			} catch (ParseException e) {
				throw new IOException("Invalid AuthzId value: " + e.getMessage(), e);
			}

		} else if (className.equals(DNIdentity.class.getName())) {

			try {
				return new DNIdentity(new DN(value));
			} catch (LDAPException e) {
				throw new IOException("Invalid DN identity value: " + e.getMessage(), e);
			}

		} else {
			throw new ClassNotFoundException("Class not found: " + className);
		}
	}
}
