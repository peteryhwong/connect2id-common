package com.nimbusds.common.id;


/**
 * Represents a user identifier (UID).
 */
public final class UID extends BaseIdentifier {

	
	/**
         * Creates a new user identifier (UID) with the specified value.
         *
         * @param value The user identifier (UID) value. Must not be 
	 *              {@code null}.
         */
        public UID(final String value) {
        
		super(value);
	}
        
        
        @Override
        public boolean equals(final Object object) {

                return object instanceof UID && this.toString().equals(object.toString());
        }
}
