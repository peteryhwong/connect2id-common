package com.nimbusds.common.appendable;


import java.io.StringWriter;

import junit.framework.TestCase;

import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;


/**
 * Tests the JSON array writer.
 */
public class JSONArrayWriterTest extends TestCase {


	public void testNullWriter() {

		try {
			new JSONArrayWriter<Subject>(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The writer must not be null", e.getMessage());
		}


		try {
			new JSONArrayWriter<Subject>(null, true);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The writer must not be null", e.getMessage());
		}
	}


	public void testWriteEmpty()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONArrayWriter jsonArrayWriter = new JSONArrayWriter(writer);

		jsonArrayWriter.writeStart();
		jsonArrayWriter.writeEnd();

		assertEquals("[]", writer.toString());
	}


	public void testWriteOneSubject()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONArrayWriter<Subject> jsonArrayWriter = new JSONArrayWriter<>(writer);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new Subject("alice"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"alice\"]", writer.toString());
	}


	public void testWriteTwoSubjects()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONArrayWriter<Subject> jsonArrayWriter = new JSONArrayWriter<>(writer);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new Subject("alice"));
		jsonArrayWriter.append(new Subject("bob"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"alice\",\"bob\"]", writer.toString());
	}


	public void testDuplicateSubjects()
		throws Exception {

		StringWriter writer = new StringWriter();
		final boolean noDuplicates = false;
		JSONArrayWriter<Subject> jsonArrayWriter = new JSONArrayWriter<>(writer, noDuplicates);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new Subject("alice"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("cindy"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"alice\",\"bob\",\"bob\",\"bob\",\"cindy\"]", writer.toString());
	}


	public void testNoDuplicateSubjects()
		throws Exception {

		StringWriter writer = new StringWriter();
		final boolean noDuplicates = true;
		JSONArrayWriter<Subject> jsonArrayWriter = new JSONArrayWriter<>(writer, noDuplicates);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new Subject("alice"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("bob"));
		jsonArrayWriter.append(new Subject("cindy"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"alice\",\"bob\",\"cindy\"]", writer.toString());
	}


	public void testWriteOneClientID()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONArrayWriter<ClientID> jsonArrayWriter = new JSONArrayWriter<>(writer);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new ClientID("1"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"1\"]", writer.toString());
	}


	public void testWriteTwoClientIDs()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONArrayWriter<ClientID> jsonArrayWriter = new JSONArrayWriter<>(writer);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new ClientID("1"));
		jsonArrayWriter.append(new ClientID("2"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"1\",\"2\"]", writer.toString());
	}


	public void testNoDuplicateClients()
		throws Exception {

		StringWriter writer = new StringWriter();
		final boolean noDuplicates = true;
		JSONArrayWriter<ClientID> jsonArrayWriter = new JSONArrayWriter<>(writer, noDuplicates);

		jsonArrayWriter.writeStart();

		jsonArrayWriter.append(new ClientID("1"));
		jsonArrayWriter.append(new ClientID("1"));
		jsonArrayWriter.append(new ClientID("2"));

		jsonArrayWriter.writeEnd();

		assertEquals("[\"1\",\"2\"]", writer.toString());
	}
}
