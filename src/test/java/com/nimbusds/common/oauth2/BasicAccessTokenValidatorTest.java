package com.nimbusds.common.oauth2;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;


/**
 * Tests the basic access token validator.
 */
public class BasicAccessTokenValidatorTest extends TestCase {


	public void testConstructor() {

		BearerAccessToken accessToken = new BearerAccessToken();

		System.out.println("Created new Bearer token: " + accessToken.getValue());

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);

		assertEquals(accessToken, validator.getAccessToken());
	}


	public void testConstructorNull() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(null);

		assertNull(validator.getAccessToken());
	}


	public void testPass() {

		BearerAccessToken accessToken = new BearerAccessToken();

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);

		validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());
	}


	public void testWebAPIDisabled() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(null);

		BearerAccessToken accessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(Response.Status.FORBIDDEN.getStatusCode(), e.getResponse().getStatus());

			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testMissingToken() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(null);

		try {
			validator.validateBearerAccessToken(null);

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.MISSING_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.MISSING_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("missing_token", (String)jsonObject.get("error"));
		}
	}


	public void testBlankToken() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(null);

		try {
			validator.validateBearerAccessToken("Authorization ");

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.MISSING_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.MISSING_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("missing_token", (String)jsonObject.get("error"));
		}
	}


	public void testBadToken() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(new BearerAccessToken());

		BearerAccessToken otherAccessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(otherAccessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("invalid_token", (String)jsonObject.get("error"));
		}
	}

}
