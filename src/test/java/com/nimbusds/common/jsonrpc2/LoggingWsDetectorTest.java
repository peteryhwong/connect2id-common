package com.nimbusds.common.jsonrpc2;


import java.net.URL;

import junit.framework.TestCase;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Tests the LoggingWsDetector class.
 */
public class LoggingWsDetectorTest extends TestCase {


	private static final String WS_URL = "http://localhost:8080/json2ldap/";
	
	
	private static final String BAD_URL = "http://localhost:8080/no-such-service/";


	private final Logger log = LogManager.getLogger(LoggingWsDetector.class);
	
	
	public void testDetectAndLogSuccess()
		throws Exception {
	
		LoggingWsDetector detector = new LoggingWsDetector(new URL(WS_URL), "Json2Ldap", log);
		
		new Thread(detector).start();
		
		// Wait 4sec for other thread
		Thread.sleep(4000);
	}
	
	
	public void testDetectAndLogBadURL()
		throws Exception {
	
		LoggingWsDetector detector = new LoggingWsDetector(new URL(BAD_URL), "Json2Ldap", log);
		
		new Thread(detector).start();
		
		// Wait 4sec for other thread
		Thread.sleep(4000);
	}
}
