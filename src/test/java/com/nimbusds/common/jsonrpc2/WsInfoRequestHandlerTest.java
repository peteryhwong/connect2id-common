package com.nimbusds.common.jsonrpc2;


import java.util.*;

import junit.framework.TestCase;

import com.thetransactioncompany.jsonrpc2.*;

import com.thetransactioncompany.jsonrpc2.server.*;


/**
 * Tests the ws.* JSON-RPC 2.0 request handler.
 */
public class WsInfoRequestHandlerTest extends TestCase {


	public void testConstructor() {
	
		WsInfoRequestHandler ws = new WsInfoRequestHandler();
		
		Set<String> methods = new HashSet<>(Arrays.asList(ws.handledRequests()));
		
		assertTrue(methods.contains("ws.getName"));
		assertTrue(methods.contains("ws.getVersion"));
		assertTrue(methods.contains("ws.getTime"));
		
		assertEquals(3, methods.size());
	}
	
	
	public void testBadInit() {
	
		WsInfoRequestHandler ws = new WsInfoRequestHandler();
	
		try {
			ws.init(null);
			
			fail("Failed to raise exception on null args");
		
		} catch (IllegalArgumentException e) {
		
			// ok
		}
	}
	
	
	public void testGetName() {
	
		WsInfoRequestHandler ws = new WsInfoRequestHandler();
		ws.init(new WsInfo("My-Web-App", "1.0"));
	
		JSONRPC2Request req = new JSONRPC2Request("ws.getName", 1);
		JSONRPC2Response res = ws.process(req, new MessageContext());
		
		assertNotNull("JSON-RPC 2.0 response must be defined", res);
		
		assertTrue("Response must indicate success", res.indicatesSuccess());
		
		assertEquals("WS name check", "My-Web-App", (String)res.getResult());
	}
	
	
	public void testGetVersion() {
	
		WsInfoRequestHandler ws = new WsInfoRequestHandler();
		ws.init(new WsInfo("My-Web-App", "1.0"));
	
		JSONRPC2Request req = new JSONRPC2Request("ws.getVersion", 1);
		JSONRPC2Response res = ws.process(req, new MessageContext());
		
		assertNotNull("JSON-RPC 2.0 response must be defined", res);
		
		assertTrue("Response must indicate success", res.indicatesSuccess());
		
		assertEquals("WS version check", "1.0", (String)res.getResult());
	}
	
	
	public void testGetTime() {
	
		WsInfoRequestHandler ws = new WsInfoRequestHandler();
		ws.init(new WsInfo("My-Web-App", "1.0"));
	
		JSONRPC2Request req = new JSONRPC2Request("ws.getTime", 1);
		JSONRPC2Response res = ws.process(req, new MessageContext());
		
		assertNotNull("JSON-RPC 2.0 response must be defined", res);
		
		String isoTime = (String)res.getResult();
		
		System.out.println("Time: " + isoTime);
		
		assertEquals("UTC timezone check", 'Z', isoTime.charAt(isoTime.length() -1));
	}
}
