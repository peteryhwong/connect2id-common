package com.nimbusds.common.jsonrpc2;


import java.net.URL;
import java.nio.charset.Charset;

import junit.framework.TestCase;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static net.jadler.Jadler.*;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;


/**
 * Tests the WsDetector.
 */
public class WsDetectorTest extends TestCase {


	private static class WsGetNameRequestMatcher extends BaseMatcher<String> {


		@Override
		public boolean matches(Object o) {

			JSONRPC2Request request;

			try {
				request = JSONRPC2Request.parse(o.toString());

			} catch (JSONRPC2ParseException e) {
				return false;
			}

			if (! request.getMethod().equals("ws.getName"))
				return false;

			return true;
		}


		@Override
		public void describeTo(Description description) {

		}
	}


	private static class WsGetVersionRequestMatcher extends BaseMatcher<String> {


		@Override
		public boolean matches(Object o) {

			JSONRPC2Request request;

			try {
				request = JSONRPC2Request.parse(o.toString());

			} catch (JSONRPC2ParseException e) {
				return false;
			}

			if (! request.getMethod().equals("ws.getVersion"))
				return false;

			return true;
		}


		@Override
		public void describeTo(Description description) {

		}
	}


	@Before
	public void setUp() {
		initJadler();
	}

	@After
	public void tearDown() {
		closeJadler();
	}
	

	@Test
	public void testSuccess()
		throws Exception {

		WsDetector detector = new WsDetector(new URL("http://localhost:" + port()));

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new WsGetNameRequestMatcher())
		.respond()
			.withStatus(200)
			.withBody(new JSONRPC2Response("Json2Ldap", 0).toJSONString())
			.withEncoding(Charset.forName("UTF-8"))
			.withContentType("application/json");

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new WsGetVersionRequestMatcher())
		.respond()
			.withStatus(200)
			.withBody(new JSONRPC2Response("3.0 (2014-08-24)", 0).toJSONString())
			.withEncoding(Charset.forName("UTF-8"))
			.withContentType("application/json");


		WsInfo wsInfo = detector.detect();
		
		System.out.println("Detected " + wsInfo);
		
		assertEquals("Json2Ldap", wsInfo.getName());
		
		assertNotNull(wsInfo.getVersion());
	}
	

	@Test
	public void testBadURL()
		throws Exception {
		
		WsDetector detector = new WsDetector(new URL("http://localhost:" + port()));

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new WsGetNameRequestMatcher())
		.respond()
			.withStatus(404)
			.withBody("Not found")
			.withEncoding(Charset.forName("UTF-8"))
			.withContentType("text/plain");
		
		try {
			detector.detect();
			fail();
			
		} catch (Exception e) {
		
			// ok
		}
	}
}
