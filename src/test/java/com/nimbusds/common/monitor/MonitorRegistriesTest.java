package com.nimbusds.common.monitor;


import junit.framework.TestCase;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.health.HealthCheck;


/**
 * Tests the shared metrics registries.
 */
public class MonitorRegistriesTest extends TestCase {


	public void testRetrieval() {

		assertNotNull(MonitorRegistries.getMetricRegistry());

		assertNotNull(MonitorRegistries.getHealthCheckRegistry());
	}


	public void testMetricRegistrySameObject() {

		MonitorRegistries.getMetricRegistry().register("test-gauge", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 100;
			}
		});

		assertEquals(100, MonitorRegistries.getMetricRegistry().getGauges().get("test-gauge").getValue());
	}


	public void testHealthCheckRegistrySameObject() {

		MonitorRegistries.getHealthCheckRegistry().register("test-check", new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.healthy();
			}
		});

		assertEquals(HealthCheck.Result.healthy(), MonitorRegistries.getHealthCheckRegistry().runHealthCheck("test-check"));
	}


	public void testRegisterMetricSetNull() {

		MonitorRegistries.register(null);
	}


	public void testRegisterMetricNull() {

		MonitorRegistries.register(null, (Metric) null);
		MonitorRegistries.register("name", (Metric) null);
		MonitorRegistries.register(null, new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 100;
			}
		});
	}


	public void testRegisterHealthCheckNull() {

		MonitorRegistries.register(null, (HealthCheck) null);
		MonitorRegistries.register("name", (HealthCheck) null);
		MonitorRegistries.register(null, new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.healthy();
			}
		});
	}


	public void testRegisterMetric() {

		MonitorRegistries.register("gauge-1", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 100;
			}
		});

		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains("gauge-1"));
	}


	public void testUpdateMetric() {

		MonitorRegistries.register("gauge-2", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 200;
			}
		});

		MonitorRegistries.register("gauge-2", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 300;
			}
		});

		assertEquals(300, MonitorRegistries.getMetricRegistry().getGauges().get("gauge-2").getValue());
	}


	public void testRemoveMetric() {

		MonitorRegistries.register("gauge-2", new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return 200;
			}
		});

		MonitorRegistries.register("gauge-2", (Metric)null);

		assertNull(MonitorRegistries.getMetricRegistry().getGauges().get("gauge-2"));
	}


	public void testRegisterHealthCheck() {

		MonitorRegistries.register("check-1", new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.unhealthy("problem");
			}
		});

		assertEquals("problem", MonitorRegistries.getHealthCheckRegistry().runHealthCheck("check-1").getMessage());
	}


	public void testUpdateHealthCheck()
		throws Exception {

		MonitorRegistries.register("check-2", new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.unhealthy("AAA");
			}
		});

		MonitorRegistries.register("check-2", new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.unhealthy("BBB");
			}
		});

		assertEquals("BBB", MonitorRegistries.getHealthCheckRegistry().runHealthCheck("check-2").getMessage());
	}


	public void testRemoveHealthCheck()
		throws Exception {

		MonitorRegistries.register("check-3", new HealthCheck() {
			@Override
			protected Result check() throws Exception {
				return Result.unhealthy("AAA");
			}
		});

		MonitorRegistries.register("check-3", (HealthCheck) null);

		assertFalse(MonitorRegistries.getHealthCheckRegistry().getNames().contains("check-3"));
	}
}
