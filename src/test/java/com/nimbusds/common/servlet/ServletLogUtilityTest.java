package com.nimbusds.common.servlet;


import junit.framework.TestCase;


/**
 * Tests the servlet log utility.
 */
public class ServletLogUtilityTest extends TestCase {


	public void testNullArguments() {

		ServletLogUtility.log(null);
		ServletLogUtility.log(null, null);
	}
}
