package com.nimbusds.common.servlet;


import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletContextEvent;

import junit.framework.TestCase;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;


/**
 * Tests the metrics launcher.
 */
public class MonitorLauncherTest extends TestCase {


	public void testContextInitialized() {

		MonitorLauncher launcher = new MonitorLauncher();

		MockServletContext servletCtx = new MockServletContext();

		String configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		String propertyString = "monitor.enableJMX: false\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute("com.codahale.metrics.servlets.MetricsServlet.registry")
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry")
			instanceof HealthCheckRegistry);

		assertNull(launcher.jmxReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}


	public void testContextInitializedWithJMXReporter() {

		MonitorLauncher launcher = new MonitorLauncher();

		MockServletContext servletCtx = new MockServletContext();

		String configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		String propertyString = "monitor.enableJMX: true\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute("com.codahale.metrics.servlets.MetricsServlet.registry")
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry")
			instanceof HealthCheckRegistry);

		assertNotNull(launcher.jmxReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}
}
