package com.nimbusds.common.servlet;


import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import junit.framework.TestCase;

import com.codahale.metrics.servlets.AdminServlet;

import com.nimbusds.oauth2.sdk.token.BearerAccessToken;

import com.nimbusds.common.monitor.MonitorRegistries;


/**
 * Tests the protected metrics servlet.
 */
public class MonitorServletTest extends TestCase {


	public void testInheritance() {

		assertTrue(new MonitorServlet() instanceof AdminServlet);
	}


	public void testAPITokenPropertyName() {

		assertEquals("monitor.apiAccessToken", MonitorServlet.API_TOKEN_PROPERTY_NAME);
	}


	public void testMinTokenLength() {

		assertEquals(32, MonitorServlet.MIN_TOKEN_LENGTH);
	}


	public void testInit()
		throws Exception {

		MonitorServlet servlet = new MonitorServlet();

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		servletConfig.servletCtx.setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		BearerAccessToken token = new BearerAccessToken(32);
		String propertyString = "monitor.apiAccessToken: " + token.getValue() + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		servlet.init(servletConfig);

		assertEquals(token, servlet.tokenValidator.getAccessToken());
	}


	public void testInitWithSystemProperty()
		throws Exception {

		MonitorServlet servlet = new MonitorServlet();

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		servletConfig.servletCtx.setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		String propertyString = "monitor.apiAccessToken: \n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		BearerAccessToken token = new BearerAccessToken(32);

		System.getProperties().setProperty("monitor.apiAccessToken", token.getValue()); // override

		servlet.init(servletConfig);

		System.getProperties().remove("monitor.apiAccessToken");

		assertEquals(token, servlet.tokenValidator.getAccessToken());
	}


	public void testInitTokenTooShort()
		throws Exception {

		MonitorServlet servlet = new MonitorServlet();

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		servletConfig.servletCtx.setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		BearerAccessToken token = new BearerAccessToken("0123456789012345678901234567890"); // 31 chars
		String propertyString = "monitor.apiAccessToken: " + token.getValue() + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		try {
			servlet.init(servletConfig);
			fail();
		} catch (Exception e) {
			assertEquals("The monitor web API access token must be at least 32 characters long", e.getMessage());
		}
	}


	public void testInitCtxParamMissing()
		throws Exception {

		MonitorServlet servlet = new MonitorServlet();

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		servletConfig.servletCtx.setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";

		BearerAccessToken token = new BearerAccessToken("01234567890123456789012345678901"); // 32 chars
		String propertyString = "monitor.apiAccessToken: " + token.getValue() + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		try {
			servlet.init(servletConfig);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet context (web.xml) init parameter: monitor.configurationFile", e.getMessage());
		}
	}


	public void testInitPropertiesFileMissing()
		throws Exception {

		MonitorServlet servlet = new MonitorServlet();

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(
			"com.codahale.metrics.servlets.MetricsServlet.registry",
			MonitorRegistries.getMetricRegistry());

		servletConfig.servletCtx.setAttribute("com.codahale.metrics.servlets.HealthCheckServlet.registry",
			MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		try {
			servlet.init(servletConfig);
			fail();
		} catch (Exception e) {
			assertEquals("Missing file or invalid path: /WEB-INF/monitor.properties", e.getMessage());
		}
	}
}
