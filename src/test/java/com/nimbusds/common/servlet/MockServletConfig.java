package com.nimbusds.common.servlet;


import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;


/**
 * Mock servlet config.
 */
public class MockServletConfig implements ServletConfig {


	public final Map<String,String> initParams = new HashMap<>();


	public String serlvetName = "MockServlet";


	public MockServletContext servletCtx = new MockServletContext();


	@Override
	public String getServletName() {

		return serlvetName;
	}


	@Override
	public ServletContext getServletContext() {

		return servletCtx;
	}


	@Override
	public String getInitParameter(String s) {

		return initParams.get(s);
	}


	@Override
	public Enumeration<String> getInitParameterNames() {

		return new Vector<>(initParams.keySet()).elements();
	}
}
