package com.nimbusds.common.servlet;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import junit.framework.TestCase;


/**
 * Resource retriever test.
 */
public class ResourceRetrieverTest extends TestCase {


	public void testGetStreamForServletCtx()
		throws Exception {

		MockServletContext servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		InputStream is = ResourceRetriever.getStream(servletCtx, "configFile", null);

		assertEquals("app.someProperty: value", IOUtils.toString(is, StandardCharsets.UTF_8));
	}


	public void testGetStringForServletCtx()
		throws Exception {

		MockServletContext servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		String s = ResourceRetriever.getString(servletCtx, "configFile", null);

		assertEquals("app.someProperty: value\n", s);
	}


	public void testGetPropertiesForServletCtx()
		throws Exception {

		MockServletContext servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		Properties props = ResourceRetriever.getProperties(servletCtx, "configFile", null);
		assertEquals("value", props.getProperty("app.someProperty"));
		assertEquals(1, props.size());
	}


	public void testGetStreamForServletCtxNoSuchResource()
		throws Exception {

		MockServletContext servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		try {
			ResourceRetriever.getStream(servletCtx, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing file or invalid path: /WEB-INF/app.properties", e.getMessage());
		}
	}


	public void testGetStreamForServletCtxNoSuchParam()
		throws Exception {

		MockServletContext servletCtx = new MockServletContext();

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		try {
			ResourceRetriever.getStream(servletCtx, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet context (web.xml) init parameter: configFile", e.getMessage());
		}
	}


	public void testGetStreamForServletConfig()
		throws Exception {

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.initParams.put("configFile", "/WEB-INF/app.properties");

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		InputStream is = ResourceRetriever.getStream(servletConfig, "configFile", null);

		assertEquals("app.someProperty: value", IOUtils.toString(is, StandardCharsets.UTF_8));
	}


	public void testGetPropertiesForServletConfig()
		throws Exception {

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.initParams.put("configFile", "/WEB-INF/app.properties");

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		Properties props = ResourceRetriever.getProperties(servletConfig, "configFile", null);
		assertEquals("value", props.getProperty("app.someProperty"));
		assertEquals(1, props.size());
	}


	public void testGetStreamForServletConfigNoSuchResource()
		throws Exception {

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.initParams.put("configFile", "/WEB-INF/app.properties");

		try {
			ResourceRetriever.getStream(servletConfig, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing file or invalid path: /WEB-INF/app.properties", e.getMessage());
		}
	}


	public void testGetStreamForServletConfigNoSuchParam()
		throws Exception {

		MockServletConfig servletConfig = new MockServletConfig();

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		try {
			ResourceRetriever.getStream(servletConfig, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet (web.xml) init parameter: configFile", e.getMessage());
		}
	}
}
