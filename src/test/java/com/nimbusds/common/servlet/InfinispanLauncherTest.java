package com.nimbusds.common.servlet;


import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletContextEvent;

import org.infinispan.manager.EmbeddedCacheManager;

import junit.framework.TestCase;


/**
 * Tests the Infinispan launcher.
 */
public class InfinispanLauncherTest extends TestCase {


	public void testConstants() {

		assertEquals("infinispan.configurationFile", InfinispanLauncher.INFINISPAN_CONFIG_FILENAME);
		assertEquals("org.infinispan.manager.EmbeddedCacheManager", InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME);
	}


	public void testInit()
		throws Exception {

		MockServletContext servletContext = new MockServletContext();

		servletContext.initParams.put(InfinispanLauncher.INFINISPAN_CONFIG_FILENAME, "/WEB-INF/infinispan.xml");

		String infinispanXmlConfig = "<infinispan/>";

		servletContext.resourceStreams.put("/WEB-INF/infinispan.xml", new ByteArrayInputStream(infinispanXmlConfig.getBytes(StandardCharsets.UTF_8)));

		InfinispanLauncher launcher = new InfinispanLauncher();

		launcher.contextInitialized(new ServletContextEvent(servletContext));

		assertTrue(servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME) instanceof EmbeddedCacheManager);

		launcher.contextDestroyed(new ServletContextEvent(servletContext));
	}
}
