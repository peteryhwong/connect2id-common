package com.nimbusds.common.id;


import junit.framework.TestCase;


/**
 * Tests the SID class.
 */
public class SIDTest extends TestCase {


	public void testDefaultConstructor() {

		SID sid = new SID();

		assertEquals(43, sid.toString().length());
	}


	public void testValueConstructor() {

		assertEquals("123", new SID("123").toString());
	}


	public void testEquality() {

		assertTrue(new SID("123").equals(new SID("123")));
	}


	public void testInequality() {

		assertFalse(new SID("abc").equals(new SID("123")));
	}
}
