package com.nimbusds.common.id;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONValue;


/**
 * Tests the username class.
 */
public class UsernameTest extends TestCase {


	public void testLegal1() {
	
		assertTrue(Username.isLegal("user.01"));
	}
	
	
	public void testLegal2() {
	
		assertTrue(Username.isLegal("user.01@mail.domain.net"));
	}
	
	
	public void testIllegal1() {
	
		assertFalse(Username.isLegal("a"));
	}
	
	
	public void testIllegal2() {
	
		assertFalse(Username.isLegal("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
	}
	
	
	public void testIllegal3() {
	
		assertFalse(Username.isLegal("a b c d"));
	}
	
	
	public void testIllegal4() {
	
		assertFalse(Username.isLegal("<user.01>"));
	}
	
	
	public void testConstructorForNullPointerException() {
	
		try {
			new Username(null);
			
			fail("Failed to raise null pointer exception");
			
		} catch (NullPointerException e) {
		
			// ok
		}
	}


	public void testEquals() {

		assertTrue(new Username("alice").equals(new Username("alice")));
	}


	public void testEqualsNull() {

		assertFalse(new Username("alice").equals(null));
	}


	public void testCompareTo() {

		List<Username> list = Arrays.asList(new Username("bob"), new Username("claire"), new Username("alice"));

		Collections.sort(list);

		assertEquals("alice", list.get(0).toString());
		assertEquals("bob", list.get(1).toString());
		assertEquals("claire", list.get(2).toString());
	}


	public void testEscapedJSON() {

		// See https://code.google.com/p/json-smart/issues/detail?id=39

		String json = "[\"\\x27\"]";

		JSONArray jsonArray = (JSONArray) JSONValue.parse(json);

		assertEquals("'", jsonArray.get(0));
	}
}
