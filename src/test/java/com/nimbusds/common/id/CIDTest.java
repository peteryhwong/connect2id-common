package com.nimbusds.common.id;


import java.util.Map;
import java.util.Hashtable;
import java.util.UUID;

import junit.framework.TestCase;
import org.apache.commons.codec.binary.Base64;


/**
 * Tests the CID class.
 */
public class CIDTest extends TestCase {
	
	
	public void testConstant() {
		
		assertEquals(32, BaseIdentifier.DEFAULT_BYTE_LENGTH);
	}
	
	
	public void testGenerator() {
		
		CID cid = new CID();
		
		System.out.println("Generated new 256-bit CID: " + cid);
		
		byte[] n = Base64.decodeBase64(cid.toString());
		
		assertEquals(BaseIdentifier.DEFAULT_BYTE_LENGTH, n.length);
	}
	
	
	public void testStringValue() {
	
		String value = UUID.randomUUID().toString();
	
		CID cid = new CID(value);
		
		assertEquals(value, cid.toString());
	}
	
	
	public void testHashcode() {
	
		String value = "123";
		
		CID cid1 = new CID(value);
		CID cid2 = new CID(value);
	
		assertEquals(cid1.hashCode(), cid2.hashCode());
	}
	
	
	public void testEquals() {
	
		String value = "123";
		
		CID cid1 = new CID(value);
		CID cid2 = new CID(value);
		
		assertTrue(cid1.equals(cid2));
	}


	public void testEqualsNull() {

		assertFalse(new CID().equals(null));
	}
	
	
	public void testHashMapUse() {
	
		Map<CID,String> hashtable = new Hashtable<>();
		
		String value = "123";
		
		CID cid = new CID(value);
		
		hashtable.put(cid, value);
		
		String valueOut = hashtable.get(cid);
		
		if (valueOut == null)
			fail("Hashmap retrieval by CID key failed");
		
		
		valueOut = hashtable.get(new CID(value));
		
		if (valueOut == null)
			fail("Hashmap retrieval by reproduced CID key failed");
	}

}
