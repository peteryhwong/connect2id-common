package com.nimbusds.common.id;


import java.io.*;
import java.util.Date;

import junit.framework.TestCase;

import com.unboundid.ldap.sdk.DN;


/**
 * Tests the identifier externaliser.
 */
public class IdentifierExternalizerTest extends TestCase {


	public void testSerializedStringLengths()
		throws Exception {

		String s = "The quick brown fox jumps over the lazy dog";

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutput oout = new ObjectOutputStream(bout);
		oout.writeUTF(s);
		oout.flush();
		System.out.println("writeUTF length: " + bout.toByteArray().length);


		bout = new ByteArrayOutputStream();
		oout = new ObjectOutputStream(bout);
		oout.writeBytes(s);
		oout.flush();
		System.out.println("writeBytes length: " + bout.toByteArray().length);


		bout = new ByteArrayOutputStream();
		oout = new ObjectOutputStream(bout);
		oout.writeChars(s);
		oout.flush();
		System.out.println("writeChars length: " + bout.toByteArray().length);

		bout = new ByteArrayOutputStream();
		oout = new ObjectOutputStream(bout);
		oout.writeObject(s);
		oout.flush();
		System.out.println("writeObject length: " + bout.toByteArray().length);
	}


	public void testSerializedDateLengths()
		throws Exception {

		Date now = new Date();

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutput oout = new ObjectOutputStream(bout);
		oout.writeObject(now);
		oout.flush();
		System.out.println("Date writeObject length: " + bout.toByteArray().length);


		bout = new ByteArrayOutputStream();
		oout = new ObjectOutputStream(bout);
		oout.writeLong(now.getTime());
		oout.flush();
		System.out.println("Date writeLong length: " + bout.toByteArray().length);
	}


	public void testInOut()
		throws Exception {

		IdentifierExternalizer ext = new IdentifierExternalizer();

		assertEquals(10000, ext.getId() + 0);

		assertTrue(ext.getTypeClasses().contains(AuthzId.class));
		assertTrue(ext.getTypeClasses().contains(CID.class));
		assertTrue(ext.getTypeClasses().contains(DNIdentity.class));
		assertTrue(ext.getTypeClasses().contains(SID.class));
		assertTrue(ext.getTypeClasses().contains(UID.class));
		assertTrue(ext.getTypeClasses().contains(Username.class));
		assertEquals(6, ext.getTypeClasses().size());


		ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
		ObjectOutput oout = new ObjectOutputStream(bout);

		AuthzId authzId = new AuthzId(new Username("alice"));
		ext.writeObject(oout, authzId);

		CID cidIn = new CID();
		ext.writeObject(oout, cidIn);

		DNIdentity dnIn = new DNIdentity(new DN("cn=Directory Manager"));
		ext.writeObject(oout, dnIn);

		SID sidIn = new SID();
		ext.writeObject(oout, sidIn);

		UID uidIn = new UID("alice");
		ext.writeObject(oout, uidIn);

		Username usernameIn = new Username("alice");
		ext.writeObject(oout, usernameIn);

		oout.flush();

		ObjectInput oin = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()));

		assertEquals(authzId, ext.readObject(oin));
		assertEquals(cidIn, ext.readObject(oin));
		assertEquals(dnIn, ext.readObject(oin));
		assertEquals(sidIn, ext.readObject(oin));
		assertEquals(uidIn, ext.readObject(oin));
		assertEquals(usernameIn, ext.readObject(oin));
	}
}
