package com.nimbusds.common.ldap;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

import net.minidev.json.JSONObject;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.util.StaticUtils;


/**
 * Tests the attributes mapper.
 */
public class AttributeMapperTest extends TestCase {


	private static Map<String,Object> getTransformMap() {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "cn");
		directive.put("langTag", true);
		map.put("name", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "personAge");
		directive.put("jsonType", "int");
		map.put("age", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "modified");
		directive.put("jsonType", "long");
		map.put("modifiedTimestamp", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "locales");
		directive.put("jsonType", "string-array");
		map.put("ui", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "active");
		directive.put("ldapType", "boolean");
		directive.put("jsonType", "boolean");
		map.put("active", directive);
		
		directive = new HashMap<>();
		directive.put("ldapAttr", "expirationTime");
		directive.put("ldapType", "time");
		directive.put("jsonType", "long");
		map.put("exp", directive);

		directive = new HashMap<>();
		directive.put("ldapValue", "TRUE");
		directive.put("jsonType", "boolean");
		map.put("verified", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "latitude");
		map.put("geo.lat", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "longitude");
		map.put("geo.long", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "scopeValue");
		directive.put("split", true);
		map.put("scope", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "isAuthenticated");
		directive.put("ldapType", "boolean");
		directive.put("jsonType", "boolean");
		directive.put("default", false);
		map.put("auth", directive);

		return map;
	}


	public void testInit()
		throws Exception {

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		List<String> ldapAttrs = Arrays.asList(mapper.getLDAPAttributeNames());
		System.out.println("Mapped LDAP attributes: " + ldapAttrs);
		assertTrue(ldapAttrs.contains("cn"));
		assertTrue(ldapAttrs.contains("personAge"));
		assertTrue(ldapAttrs.contains("modified"));
		assertTrue(ldapAttrs.contains("locales"));
		assertTrue(ldapAttrs.contains("active"));
		assertTrue(ldapAttrs.contains("expirationTime"));
		assertTrue(ldapAttrs.contains("latitude"));
		assertTrue(ldapAttrs.contains("longitude"));
		assertTrue(ldapAttrs.contains("scopeValue"));
		assertTrue(ldapAttrs.contains("isAuthenticated"));
		assertEquals(10, ldapAttrs.size());

		// check name <-> cn mapping
		assertEquals("cn", mapper.getLDAPAttributeName("name"));

		// check non-existing mapping
		assertNull(mapper.getLDAPAttributeName("no-such-attribute"));

		// check selected mappings
		List<String> targetAttrs = Arrays.asList("name", "age", "ui");
		List<String> srcAttrs = mapper.getLDAPAttributeNames(targetAttrs);
		assertEquals("cn", srcAttrs.get(0));
		assertEquals("personAge", srcAttrs.get(1));
		assertEquals("locales", srcAttrs.get(2));
	}


	public void testTransformJSONObject()
		throws Exception {

		Map<String,Object> in = new HashMap<>();

		in.put("cn", Arrays.asList("Alice Adams"));

		in.put("personAge", Arrays.asList("33"));

		in.put("modified", Arrays.asList("1000200030004000"));

		in.put("locales", Arrays.asList("en", "de", "fr"));

		in.put("active", Arrays.asList("true"));
		
		long now = new Date().getTime() / 1000; // ms to sec
		in.put("expirationTime", Arrays.asList(StaticUtils.encodeGeneralizedTime(new Date(now*1000))));

		in.put("longitude", Arrays.asList("123.456"));

		in.put("latitude", Arrays.asList("456.789"));

		in.put("scopeValue", Arrays.asList("openid", "email", "profile"));

		in.put("auth", false); // matches default

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		System.out.println("[LDAP entry as JSON object] Attribute mapper in: " + JSONObject.toJSONString(in));

		JSONObject out = mapper.transform(in);

		System.out.println("[LDAP entry as JSON object] Attribute mapper out: " + JSONObject.toJSONString(out));

		assertEquals("Alice Adams", (String)out.get("name"));

		List<String> uiList = (List<String>)out.get("ui");
		assertEquals("en", uiList.get(0));
		assertEquals("de", uiList.get(1));
		assertEquals("fr", uiList.get(2));
		assertEquals(3, uiList.size());

		assertEquals(33, ((Integer)out.get("age")).intValue());
		assertTrue((Boolean)out.get("active"));

		assertEquals(1000200030004000l, ((Long)out.get("modifiedTimestamp")).longValue());
		
		assertEquals(now, ((Long)out.get("exp")).longValue());

		assertTrue((Boolean)out.get("verified"));

		JSONObject geo = (JSONObject)out.get("geo");
		assertEquals("123.456", (String)geo.get("long"));
		assertEquals("456.789", (String)geo.get("lat"));
		assertEquals(2, geo.size());

		String scope = (String)out.get("scope");
		assertEquals("openid email profile", scope);

		assertNull(out.get("isAuthenticated"));

		assertEquals(9, out.size());
	}


	public void testTransformLDAPEntry()
		throws Exception {

		long now = new Date().getTime() / 1000; // ms to sec
		
		Entry in = new Entry(
			"dn: cn=Alice Adams",
			"objectClass: top",
			"cn: Alice Adams",
			"cn;lang-en: Alice Adams (EN)",
			"personAge: 33",
			"modified: 1000200030004000",
			"locales: en",
			"locales: de",
			"locales: fr",
			"active: TRUE",
			"expirationTime: " + StaticUtils.encodeGeneralizedTime(new Date(now*1000)),
			"longitude: 123.456",
			"latitude: 456.789",
			"scopeValue: openid",
			"scopeValue: email",
			"scopeValue: profile");

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		System.out.println("[LDAP entry] Attribute mapper in: " + in);

		JSONObject out = mapper.transform(in);

		System.out.println("[LDAP entry] Attribute mapper out: " + JSONObject.toJSONString(out));

		assertEquals("Alice Adams", (String)out.get("name"));
		assertEquals("Alice Adams (EN)", (String)out.get("name#en"));

		List<String> uiList = (List<String>)out.get("ui");
		assertEquals("en", uiList.get(0));
		assertEquals("de", uiList.get(1));
		assertEquals("fr", uiList.get(2));
		assertEquals(3, uiList.size());

		assertEquals(33, ((Integer)out.get("age")).intValue());
		assertTrue((Boolean)out.get("active"));

		assertEquals(1000200030004000l, ((Long)out.get("modifiedTimestamp")).longValue());
		
		assertEquals(now, ((Long)out.get("exp")).longValue());

		assertTrue((Boolean)out.get("verified"));

		JSONObject geo = (JSONObject)out.get("geo");
		assertEquals("123.456", (String)geo.get("long"));
		assertEquals("456.789", (String)geo.get("lat"));
		assertEquals(2, geo.size());

		String scope = (String)out.get("scope");
		Set<String> scopeSet = new HashSet<>(Arrays.asList(scope.split(" ")));
		assertTrue(scopeSet.contains("openid"));
		assertTrue(scopeSet.contains("email"));
		assertTrue(scopeSet.contains("profile"));
		assertEquals(3, scopeSet.size());

		assertEquals(10, out.size());
	}


	public void testSplitPattern() {

		String[] values = AttributeMapper.SPLIT_PATTERN.split("openid email profile");

		assertEquals(3, values.length);
	}


	public void testReverseTransform()
		throws Exception {
		
		long now = new Date().getTime() / 1000; // ms to sec

		JSONObject in = new JSONObject();
		in.put("name", "Alice Adams");
		in.put("name#en", "Alice Adams (EN)");

		in.put("ui", Arrays.asList("en", "de", "fr"));
		
		in.put("age", 33);
		in.put("modifiedTimestamp", 1000200030004000l);
		in.put("active", true);
		in.put("exp", now);
		in.put("verified", true);
		in.put("scope", "openid email profile");

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		System.out.println("[Reverse transform] Attribute mapper in: " + in);

		List<Attribute> out = mapper.reverseTransform(in);

		Entry entry = new Entry("cn=Alice", out);

		System.out.println("[Reverse transform] Attribute mapper out: " + entry);

		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals("Alice Adams (EN)", entry.getAttributeValue("cn;lang-en"));
		assertEquals("33", entry.getAttributeValue("personAge"));
		assertEquals("1000200030004000", entry.getAttributeValue("modified"));
		assertEquals(3, entry.getAttributeValues("locales").length);
		assertEquals("TRUE", entry.getAttributeValue("active"));
		assertEquals(StaticUtils.encodeGeneralizedTime(new Date(now*1000)),
			     entry.getAttributeValue("expirationTime"));

		assertTrue(new HashSet<>(Arrays.asList("openid", "email", "profile")).containsAll(
			Arrays.asList(entry.getAttribute("scopeValue").getValues())));
		assertEquals(3, entry.getAttribute("scopeValue").size());
	}
	
	
	public void testReverseTransformWithLangTags()
		throws Exception {
		
		JSONObject in = new JSONObject();
		in.put("name#en", "apple");
		in.put("name#de", "Apfel");
		in.put("name#fr", "pomme");
		
		AttributeMapper mapper = new AttributeMapper(getTransformMap());
		
		List<Attribute> out = mapper.reverseTransform(in);
		
		Entry entry = new Entry("cn=Fruit", out);
		System.out.println(entry.toLDIFString());
		
		List<Attribute> attrsWithOptions = entry.getAttributesWithOptions("cn", null);
		assertEquals(3, attrsWithOptions.size());
	}


	public void testReverseTransformWithZeroTime()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("expirationTime", 0l);

		Map<String,Object> trMap = getTransformMap();
		trMap.remove("verified"); // remove preset attribute

		AttributeMapper mapper = new AttributeMapper(trMap);

		List<Attribute> out = mapper.reverseTransform(in);
		assertTrue(out.isEmpty());
	}


	public void testReverseTransformEmptyJSONArray()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("ui", "");

		Map<String,Object> trMap = getTransformMap();
		trMap.remove("verified"); // remove preset attribute

		AttributeMapper mapper = new AttributeMapper(trMap);

		List<Attribute> out = mapper.reverseTransform(in);
		assertTrue(out.isEmpty());
	}


	public void testReverseTransformEmptyLangTagStringValue()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("name", new ArrayList<String>());

		Map<String,Object> trMap = getTransformMap();
		trMap.remove("verified"); // remove preset attribute

		AttributeMapper mapper = new AttributeMapper(trMap);

		List<Attribute> out = mapper.reverseTransform(in);
		assertTrue(out.isEmpty());
	}


	public void testReverseTransformEmptyString()
		throws Exception {

		JSONObject in = new JSONObject();
		JSONObject geo = new JSONObject();
		geo.put("lat", "");
		in.put("geo", geo);

		Map<String,Object> trMap = getTransformMap();
		trMap.remove("verified"); // remove preset attribute

		AttributeMapper mapper = new AttributeMapper(trMap);

		List<Attribute> out = mapper.reverseTransform(in);
		assertTrue(out.isEmpty());
	}
	
	
	public void testLDAPLangTags()
		throws Exception {
		
		Entry ldapEntry = new Entry("dn: cn=Fruit",
			"cn;lang-de: Apfel",
			"cn;lang-fr: pomme",
			"cn;lang-en: apple");
		
		assertFalse(ldapEntry.hasAttribute("cn"));
		assertTrue(ldapEntry.hasAttribute("cn;lang-de"));
		assertTrue(ldapEntry.hasAttribute("cn;lang-fr"));
		assertTrue(ldapEntry.hasAttribute("cn;lang-en"));
		
		List<Attribute> attrList = ldapEntry.getAttributesWithOptions("cn", null);
		
		assertEquals(3, attrList.size());
		
		for (Attribute a: attrList) {
			
			System.out.println("Base attribute name: " + a.getBaseName());
			System.out.println("Attribute option: " + a.getOptions().iterator().next());
		}
	}


	public void testNestedWithLangTag()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "ct");
		directive.put("langTag", true);
		map.put("address.country", directive);

		directive = new HashMap<>();
		directive.put("ldapAttr", "l");
		directive.put("langTag", true);
		map.put("address.city", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		Entry ldapEntry = new Entry("dn: cn=Alice",
			"ct;lang-en: Bulgaria",
			"ct;lang-bg: Bulgaria (BG)",
			"l;lang-en: Sofia",
			"l;lang-bg: Sofia (BG)");

		System.out.println("Attribute mapper in: " + ldapEntry);

		JSONObject out = mapper.transform(ldapEntry);

		System.out.println("Attribute mapper out: " + JSONObject.toJSONString(out));

		JSONObject en = (JSONObject)out.get("address#en");
		assertEquals("Bulgaria", (String)en.get("country"));
		assertEquals("Sofia", (String)en.get("city"));
		assertEquals(2, en.size());

		JSONObject bg = (JSONObject)out.get("address#bg");
		assertEquals("Bulgaria (BG)", (String)bg.get("country"));
		assertEquals("Sofia (BG)", (String)bg.get("city"));
		assertEquals(2, bg.size());

		assertEquals(2, out.size());
	}


	public void testRegEx()
		throws Exception {

		DN dn = new DN("cn=admins,ou=groups,dc=wonderland,dc=net");

		System.out.println("DN in: " + dn);

		Pattern p = Pattern.compile("cn=([a-zA-Z_0-9 ]+),.*");

		Matcher m = p.matcher(dn.toString());

		assertTrue("Group name matched", m.matches());

		assertEquals("Group count", 1, m.groupCount());

		assertEquals("Regex match 1", "admins", m.group(1));
	}


	public void testRegExMatcherDirectiveOnString()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "supervisor");
		directive.put("reMatch", "uid=([a-zA-Z_0-9 ]+),.*");
		map.put("supervisor", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		Entry ldapEntry = new Entry("dn: cn=Alice",
			"supervisor: uid=bsmith,ou=groups,dc=wonderland,dc=net");

		System.out.println("Attribute mapper in: " + ldapEntry);

		JSONObject out = mapper.transform(ldapEntry);

		assertEquals(1, out.size());

		System.out.println("Attribute mapper out: " + JSONObject.toJSONString(out));

		assertEquals("bsmith", (String) out.get("supervisor"));
		assertEquals(1, out.size());
	}


	public void testRegExMatcherDirectiveOnStringArray()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "isMemberOf");
		directive.put("jsonType", "string-array");
		directive.put("reMatch", "cn=([a-zA-Z_0-9 ]+),.*");
		map.put("groups", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		Entry ldapEntry = new Entry("dn: cn=Alice",
			"isMemberOf: cn=admins,ou=groups,dc=wonderland,dc=net",
			"isMemberOf: cn=auditors,ou=groups,dc=wonderland,dc=net");

		System.out.println("Attribute mapper in: " + ldapEntry);

		JSONObject out = mapper.transform(ldapEntry);

		assertEquals(1, out.size());

		System.out.println("Attribute mapper out: " + JSONObject.toJSONString(out));

		List<String> groups = (List<String>)out.get("groups");
		assertEquals("admins", groups.get(0));
		assertEquals("auditors", groups.get(1));
		assertEquals(2, groups.size());
	}


	public void testBadRegExMatcher() {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "isMemberOf");
		directive.put("jsonType", "string-array");
		directive.put("reMatch", "cn=(");
		map.put("groups", directive);

		try {
			new AttributeMapper(map);

			fail("Failed to raise exception");

		} catch (IllegalArgumentException e) {

			// ok
			System.out.println(e.getMessage());
		}
	}


	public void testRegExMatcherMissingGroup() {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "isMemberOf");
		directive.put("jsonType", "string-array");
		directive.put("reMatch", "cn=uid");
		map.put("groups", directive);

		try {
			new AttributeMapper(map);

			fail("Failed to raise exception");

		} catch (IllegalArgumentException e) {

			// ok
			System.out.println(e.getMessage());
		}
	}


	public void testRegExMatcherTooManyGroups() {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "isMemberOf");
		directive.put("jsonType", "string-array");
		directive.put("reMatch", "cn=(.*),ou=(.*)");
		map.put("groups", directive);

		try {
			new AttributeMapper(map);

			fail("Failed to raise exception");

		} catch (IllegalArgumentException e) {

			// ok
			System.out.println(e.getMessage());
		}
	}


	public void testSplit() {

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		JSONObject in = new JSONObject();

		in.put("scope", "openid email profile");

		Entry entry = new Entry("cn=Alice", mapper.reverseTransform(in));

		System.out.println("[Split test] Entry: " + entry.toLDIFString());

		Set<String> scopeValues = new HashSet<>(Arrays.asList(entry.getAttribute("scopeValue").getValues()));

		assertTrue(new HashSet<>(Arrays.asList("openid", "email", "profile")).containsAll(scopeValues));
		assertEquals(3, scopeValues.size());
	}


	public void testJoin()
		throws Exception {

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		Entry in = new Entry(
			"dn: cn=Alice",
			"scopeValue: openid",
			"scopeValue: email",
			"scopeValue: profile");

		JSONObject out = mapper.transform(in);

		System.out.println("[Join test] JSON object: " + out.toJSONString());

		assertEquals("openid email profile", (String) out.get("scope"));
		assertTrue((Boolean) out.get("verified"));
		assertEquals(2, out.size());
	}


	public void testTransformBooleanDefaultValueMatching()
		throws Exception {

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		Entry in = new Entry(
			"dn: cn=Alice",
			"isAuthenticated: FALSE");

		JSONObject out = mapper.transform(in);

		assertNull(out.get("auth"));
	}


	public void testTransformBooleanDefaultValueNotMatching()
		throws Exception {

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		Entry in = new Entry(
			"dn: cn=Alice",
			"isAuthenticated: TRUE");

		JSONObject out = mapper.transform(in);

		assertTrue((Boolean) out.get("auth"));
	}


	public void testReverseTransformBooleanDefaultValueMatching()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("auth", false);

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));

		assertFalse(entry.hasAttribute("isAuthenticated"));
	}


	public void testReverseTransformBooleanDefaultValueNotMatching()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("auth", true);

		AttributeMapper mapper = new AttributeMapper(getTransformMap());

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));

		assertTrue(entry.hasAttributeValue("isAuthenticated", "TRUE"));
		assertEquals("TRUE", entry.getAttributeValue("isAuthenticated"));
	}


	public void testTransformStringDefaultValueMatching()
		throws Exception {

		Map<String,Object> transformMap = getTransformMap();
		Map<String,Object> dir = new HashMap<>();
		dir.put("ldapAttr", "applicationType");
		dir.put("default", "web");
		transformMap.put("appType", dir);
		AttributeMapper mapper = new AttributeMapper(transformMap);

		Entry in = new Entry(
			"dn: cn=Alice",
			"applicationType: web");

		JSONObject out = mapper.transform(in);

		assertNull(out.get("appType"));
	}


	public void testTransformStringDefaultValueNotMatching()
		throws Exception {

		Map<String,Object> transformMap = getTransformMap();
		Map<String,Object> dir = new HashMap<>();
		dir.put("ldapAttr", "applicationType");
		dir.put("default", "web");
		transformMap.put("appType", dir);
		AttributeMapper mapper = new AttributeMapper(transformMap);

		Entry in = new Entry(
			"dn: cn=Alice",
			"applicationType: native");

		JSONObject out = mapper.transform(in);

		assertEquals("native", (String) out.get("appType"));
	}


	public void testReverseTransformStringDefaultValueMatching()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("appType", "web");

		Map<String,Object> transformMap = getTransformMap();
		Map<String,Object> dir = new HashMap<>();
		dir.put("ldapAttr", "applicationType");
		dir.put("default", "web");
		transformMap.put("appType", dir);
		AttributeMapper mapper = new AttributeMapper(transformMap);

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));

		assertFalse(entry.hasAttribute("applicationType"));
	}


	public void testReverseTransformStringDefaultValueNotMatching()
		throws Exception {

		JSONObject in = new JSONObject();
		in.put("appType", "native");

		Map<String,Object> transformMap = getTransformMap();
		Map<String,Object> dir = new HashMap<>();
		dir.put("ldapAttr", "applicationType");
		dir.put("default", "web");
		transformMap.put("appType", dir);
		AttributeMapper mapper = new AttributeMapper(transformMap);

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));

		assertTrue(entry.hasAttributeValue("applicationType", "native"));
	}


	public void testBooleanTransform()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "authzIssueRefreshToken");
		directive.put("ldapType", "boolean");
		directive.put("jsonType", "boolean");
		map.put("issue_refresh_token", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		// From upper case
		Entry in = new Entry(
			"dn: cn=Alice",
			"authzIssueRefreshToken: TRUE");

		JSONObject out = mapper.transform(in);

		assertTrue((Boolean)out.get("issue_refresh_token"));

		// From lower case
		in = new Entry(
			"dn: cn=Alice",
			"authzIssueRefreshToken: true");

		out = mapper.transform(in);

		assertTrue((Boolean)out.get("issue_refresh_token"));
	}


	public void testBooleanTransformImplicitLDAPType()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "authzIssueRefreshToken");
		directive.put("jsonType", "boolean");
		map.put("issue_refresh_token", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		// From upper case
		Entry in = new Entry(
			"dn: cn=Alice",
			"authzIssueRefreshToken: TRUE");

		JSONObject out = mapper.transform(in);

		assertTrue((Boolean)out.get("issue_refresh_token"));

		// From lower case
		in = new Entry(
			"dn: cn=Alice",
			"authzIssueRefreshToken: true");

		out = mapper.transform(in);

		assertTrue((Boolean)out.get("issue_refresh_token"));
	}


	public void testBooleanReverseTransform()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "authzIssueRefreshToken");
		directive.put("ldapType", "boolean");
		directive.put("jsonType", "boolean");
		map.put("issue_refresh_token", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		// Test TRUE
		JSONObject in = new JSONObject();
		in.put("issue_refresh_token", true);

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));
		assertTrue(entry.getAttributeValueAsBoolean("authzIssueRefreshToken"));
		assertEquals("TRUE", entry.getAttributeValue("authzIssueRefreshToken"));

		// Test FALSE
		in = new JSONObject();
		in.put("issue_refresh_token", false);

		entry = new Entry("cn=alice", mapper.reverseTransform(in));
		assertFalse(entry.getAttributeValueAsBoolean("authzIssueRefreshToken"));
		assertEquals("FALSE", entry.getAttributeValue("authzIssueRefreshToken"));
	}


	public void testBooleanReverseTransformImplicitLDAPType()
		throws Exception {

		Map<String,Object> map = new LinkedHashMap<>();

		Map<String,Object> directive = new HashMap<>();
		directive.put("ldapAttr", "authzIssueRefreshToken");
		directive.put("jsonType", "boolean");
		map.put("issue_refresh_token", directive);

		AttributeMapper mapper = new AttributeMapper(map);

		// Test TRUE
		JSONObject in = new JSONObject();
		in.put("issue_refresh_token", true);

		Entry entry = new Entry("cn=alice", mapper.reverseTransform(in));
		assertTrue(entry.getAttributeValueAsBoolean("authzIssueRefreshToken"));
		assertEquals("TRUE", entry.getAttributeValue("authzIssueRefreshToken"));

		// Test FALSE
		in = new JSONObject();
		in.put("issue_refresh_token", false);

		entry = new Entry("cn=alice", mapper.reverseTransform(in));
		assertFalse(entry.getAttributeValueAsBoolean("authzIssueRefreshToken"));
		assertEquals("FALSE", entry.getAttributeValue("authzIssueRefreshToken"));
	}
}
