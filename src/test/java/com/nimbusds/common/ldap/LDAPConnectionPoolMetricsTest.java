package com.nimbusds.common.ldap;


import java.util.Map;

import static org.junit.Assert.*;

import org.junit.Test;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionPool;

import com.nimbusds.common.monitor.MonitorRegistries;


/**
 * Tests the LDAP connection pool metrics.
 */
public class LDAPConnectionPoolMetricsTest extends TestWithLDAPServer {


	@Test
	public void testRun()
		throws Exception {

		LDAPConnectionPool pool = new LDAPConnectionPool(new LDAPConnection("localhost", testLDAPServer.getListenPort()), 1, 10);

		final String prefix = "authzStore.ldapConnector";

		MetricSet metricSet = new LDAPConnectionPoolMetrics(pool, prefix);

		Map<String,Metric> metricMap = metricSet.getMetrics();

		assertTrue(metricMap.get(prefix + ".maxAvailableConnections") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numAvailableConnections") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedDefunct") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedExpired") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedUnneeded") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numFailedCheckouts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numFailedConnectionAttempts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numReleasedValid") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckouts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsNewConnection") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsWithoutWaiting") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsAfterWaiting") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulConnectionAttempts") instanceof Gauge);

		assertEquals(13, metricMap.size());

		// Test register
		MonitorRegistries.register(metricSet);

		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".maxAvailableConnections"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numAvailableConnections"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedDefunct"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedExpired"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedUnneeded"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numFailedCheckouts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numFailedConnectionAttempts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numReleasedValid"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckouts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsNewConnection"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsWithoutWaiting"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsAfterWaiting"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulConnectionAttempts"));
	}
}
