package com.nimbusds.common.ldap;


import java.util.HashSet;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;


/**
 * Tests the JSON LDAP result formatter.
 */
public class JSONResultFormatterTest extends TestCase {


	public void testFormat()
		throws Exception {

		String dnString = "uid=alice,ou=people,dc=wonderland,dc=net";

		Entry entry = new Entry(new DN(dnString));

		entry.addAttribute("uid", "alice");
		entry.addAttribute("cn", "Alice Adams");
		entry.addAttribute("sn", "Adams");
		entry.addAttribute("givenName", "Alice");
		entry.addAttribute("mail", "alice@wonderland.net", "alice@mail.bg");


		boolean omitDN = false;
		boolean normalize = false;

		Map<String,Object> formattedEntry = 
			JSONResultFormatter.formatEntry(entry, new HashSet<String>(), omitDN, normalize);

		assertEquals(6, formattedEntry.size());

		assertEquals(dnString, (String)formattedEntry.get("DN"));

		List<String> values = (List<String>)formattedEntry.get("uid");
		assertEquals(1, values.size());
		assertEquals("alice", values.get(0));

		values = (List<String>)formattedEntry.get("cn");
		assertEquals(1, values.size());
		assertEquals("Alice Adams", values.get(0));

		values = (List<String>)formattedEntry.get("sn");
		assertEquals(1, values.size());
		assertEquals("Adams", values.get(0));

		values = (List<String>)formattedEntry.get("givenName");
		assertEquals(1, values.size());
		assertEquals("Alice", values.get(0));

		values = (List<String>)formattedEntry.get("mail");
		assertEquals(2, values.size());
		assertEquals("alice@wonderland.net", values.get(0));
		assertEquals("alice@mail.bg", values.get(1));
	}


	public void testFormatOmitDN()
		throws Exception {

		String dnString = "uid=alice,ou=people,dc=wonderland,dc=net";

		Entry entry = new Entry(new DN(dnString));

		entry.addAttribute("uid", "alice");
		entry.addAttribute("cn", "Alice Adams");


		boolean omitDN = true;
		boolean normalize = false;

		Map<String,Object> formattedEntry = 
			JSONResultFormatter.formatEntry(entry, new HashSet<String>(), omitDN, normalize);

		assertEquals(2, formattedEntry.size());

		assertNull(formattedEntry.get("DN"));

		List<String> values = (List<String>)formattedEntry.get("uid");
		assertEquals(1, values.size());
		assertEquals("alice", values.get(0));

		values = (List<String>)formattedEntry.get("cn");
		assertEquals(1, values.size());
		assertEquals("Alice Adams", values.get(0));
	}


	public void testFormatNormalizeAttributeNames()
		throws Exception {

		String dnString = "uid=alice,ou=people,dc=wonderland,dc=net";

		Entry entry = new Entry(new DN(dnString));

		entry.addAttribute("uid", "alice");
		entry.addAttribute("givenName", "Alice");
		entry.addAttribute("userPassword", "secret");

		boolean omitDN = false;
		boolean normalize = true;

		Map<String,Object> formattedEntry = 
			JSONResultFormatter.formatEntry(entry, new HashSet<String>(), omitDN, normalize);

		assertEquals(4, formattedEntry.size());

		assertEquals(dnString, (String)formattedEntry.get("DN"));

		List<String> values = (List<String>)formattedEntry.get("uid");
		assertEquals(1, values.size());
		assertEquals("alice", values.get(0));

		values = (List<String>)formattedEntry.get("givenname");
		assertEquals(1, values.size());
		assertEquals("Alice", values.get(0));

		values = (List<String>)formattedEntry.get("userpassword");
		assertEquals(1, values.size());
		assertEquals("secret", values.get(0));
	}
}