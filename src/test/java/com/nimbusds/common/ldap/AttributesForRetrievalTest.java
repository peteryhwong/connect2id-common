package com.nimbusds.common.ldap;


import java.util.*;

import junit.framework.TestCase;

import com.thetransactioncompany.jsonrpc2.*;
import com.thetransactioncompany.jsonrpc2.util.*;


/**
 * Tests the attributes for retrieval set.
 */
public class AttributesForRetrievalTest extends TestCase {


	public void testDefault() {
	
		Map<String,Object> emptyMap = new HashMap<>();
		
		NamedParamsRetriever params = new NamedParamsRetriever(emptyMap);
	
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNull(spec);
	}
	
	
	public void testEmpty() {
	
		Map<String,Object> map = new HashMap<>();
		map.put("attributes", Collections.EMPTY_LIST);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(1, spec.length);
		assertEquals("1.1", spec[0]);
	}
	
	
	public void testSelected() {
	
		Map<String,Object> map = new HashMap<>();
		List<String> names = new LinkedList<>();
		names.add("cn");
		names.add("sn");
		map.put("attributes", names);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(2, spec.length);
		assertTrue(spec[0].equals("cn") || spec[0].equals("sn"));
		assertTrue(spec[1].equals("cn") || spec[1].equals("sn"));
	}
	
	
	public void testAllUserExplicit() {
	
		Map<String,Object> map = new HashMap<>();
		List<String> names = new LinkedList<>();
		names.add("*");
		map.put("attributes", names);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(1, spec.length);
		assertEquals("*", spec[0]);
	}
	
	
	public void testOperational() {
	
		Map<String,Object> map = new HashMap<>();
		List<String> names = new LinkedList<>();
		names.add("+");
		map.put("attributes", names);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(1, spec.length);
		assertEquals("+", spec[0]);
	}
	
	
	public void testSelectedWithOperational() {
	
		Map<String,Object> map = new HashMap<>();
		List<String> names = new LinkedList<>();
		names.add("cn");
		names.add("+");
		map.put("attributes", names);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(2, spec.length);
		assertTrue(spec[0].equals("cn") || spec[0].equals("+"));
		assertTrue(spec[1].equals("cn") || spec[1].equals("+"));
	}
	
	
	public void testLowerCaseNormalization() {
	
		Map<String,Object> map = new HashMap<>();
		
		List<String> names = new LinkedList<>();
		names.add("CN");
		map.put("attributes", names);
		
		names = new LinkedList<>();
		names.add("jpegPhoto");
		map.put("binaryAttributes", names);
		
		NamedParamsRetriever params = new NamedParamsRetriever(map);
		
		AttributesForRetrieval attrs = new AttributesForRetrieval();
		
		try {
			attrs.parse(params);
			
		} catch (JSONRPC2Error e) {
		
			fail(e.getMessage());
		}
		
		String[] spec = attrs.getSpec();
		
		assertNotNull(spec);
		assertEquals(2, spec.length);
		assertTrue(spec[0].equals("cn") || spec[0].equals("jpegphoto"));
		assertTrue(spec[1].equals("cn") || spec[1].equals("jpegphoto"));
		
		Set<String> binaryNames = attrs.getBinaryAttributes();
		assertEquals(1, binaryNames.size());
		assertTrue(binaryNames.contains("jpegphoto"));
	}
}
