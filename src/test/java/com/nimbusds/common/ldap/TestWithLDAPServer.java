package com.nimbusds.common.ldap;


import org.junit.After;
import org.junit.Before;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;


/**
 * Test with LDAP server.
 */
public abstract class TestWithLDAPServer {


	/**
	 * The test in-memory LDAP server.
	 */
	protected InMemoryDirectoryServer testLDAPServer;


	@Before
	public void setUp()
		throws Exception {

		// Set test LDAP server port
		InMemoryListenerConfig listenerConfig = InMemoryListenerConfig.createLDAPConfig("test-ldap");

		// Set test LDAP server context, user
		InMemoryDirectoryServerConfig dirConfig = new InMemoryDirectoryServerConfig("dc=wonderland,dc=net");
		dirConfig.setListenerConfigs(listenerConfig);
		dirConfig.setEnforceAttributeSyntaxCompliance(true);

		// Start test LDAP server
		testLDAPServer = new InMemoryDirectoryServer(dirConfig);
		testLDAPServer.startListening();
	}


	@After
	public void tearDown() {

		testLDAPServer.shutDown(true);
	}
}
