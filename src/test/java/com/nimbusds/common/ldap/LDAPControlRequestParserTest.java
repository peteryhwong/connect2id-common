package com.nimbusds.common.ldap;


import java.util.*;

import junit.framework.TestCase;

import com.unboundid.ldap.sdk.controls.ServerSideSortRequestControl;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import com.unboundid.ldap.sdk.controls.VirtualListViewRequestControl;

import com.unboundid.util.Base64;


/**
 * Tests the LDAP control request parser.
 */
public class LDAPControlRequestParserTest extends TestCase {


	public void testServerSideSortRequestControl()
		throws Exception {

		List<Object> request = new LinkedList<>();

		Map<String,Object> sortKey = new HashMap<>();
		sortKey.put("key", "sn");
		sortKey.put("reverseOrder", false);
		sortKey.put("orderingRule", null);
		request.add(sortKey);

		sortKey = new HashMap<>();
		sortKey.put("key", "givenName");
		sortKey.put("reverseOrder", false);
		sortKey.put("orderingRule", null);
		request.add(sortKey);

		ServerSideSortRequestControl control = 
			LDAPControlRequestParser.parseServerSideSortRequestControl(request);


		assertEquals(2, control.getSortKeys().length);
		assertTrue(control.isCritical());
		
		assertEquals("sn", control.getSortKeys()[0].getAttributeName());
		assertFalse(control.getSortKeys()[0].reverseOrder());
		assertNull(control.getSortKeys()[0].getMatchingRuleID());
		
		assertEquals("givenName", control.getSortKeys()[1].getAttributeName());
		assertFalse(control.getSortKeys()[1].reverseOrder());
		assertNull(control.getSortKeys()[1].getMatchingRuleID());
	
	}


	public void testSimplePagedResultsControl()
		throws Exception {

		Map<String,Object> request = new HashMap<>();
		request.put("size", 25);
		request.put("cookie", "AAAAAAAAABw=");

		SimplePagedResultsControl control =
			LDAPControlRequestParser.parseSimplePagedResultsControl(request);


		assertTrue(control.isCritical());

		assertEquals(25, control.getSize());
		assertEquals("AAAAAAAAABw=", Base64.encode(control.getCookie().getValue()));
	}


	public void testVirtualListViewControlMinSpec()
		throws Exception {

		Map<String,Object> request = new HashMap<>();
		request.put("after", 9);

		VirtualListViewRequestControl control =
			LDAPControlRequestParser.parseVirtualListViewControl(request);


		assertTrue(control.isCritical());

		assertEquals(1, control.getTargetOffset());
		assertEquals(0, control.getBeforeCount());
		assertEquals(9, control.getAfterCount());
		assertEquals(0, control.getContentCount());
		assertNull(control.getContextID());
	}


	public void testVirtualListViewControlFullSpec()
		throws Exception {

		Map<String,Object> request = new HashMap<>();
		request.put("offset", 12);
		request.put("before", 1);
		request.put("after", 9);
		request.put("totalEntryCount", 123);
		request.put("cookie", "AAAAAAAAABw=");

		VirtualListViewRequestControl control =
			LDAPControlRequestParser.parseVirtualListViewControl(request);


		assertTrue(control.isCritical());

		assertEquals(12, control.getTargetOffset());
		assertEquals(1, control.getBeforeCount());
		assertEquals(9, control.getAfterCount());
		assertEquals(123, control.getContentCount());
		assertEquals("AAAAAAAAABw=", Base64.encode(control.getContextID().getValue()));
	}
}