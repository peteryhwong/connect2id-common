package com.nimbusds.common.ldap;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.codahale.metrics.health.HealthCheck;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;


/**
 * Tests the LDAP health check.
 */
public class LDAPHealthCheckTest extends TestWithLDAPServer {


	@Override
	public void setUp()
		throws Exception {

		super.setUp();

		Entry entry = new Entry("dc=wonderland,dc=net");
		entry.addAttribute("objectClass", "top", "domain");
		entry.addAttribute("dc", "wonderland");
		testLDAPServer.add(entry);

		// Create test LDAP entry
		entry = new Entry("ou=test,dc=wonderland,dc=net");
		entry.addAttribute("objectClass", "top", "organizationalUnit");
		entry.addAttribute("ou", "test");
		testLDAPServer.add(entry);
	}


	@Test
	public void testHappy()
		throws Exception {

		LDAPHealthCheck healthCheck = new LDAPHealthCheck(testLDAPServer, new DN("ou=test,dc=wonderland,dc=net"), null);

		HealthCheck.Result result = healthCheck.check();

		assertTrue(result.isHealthy());
	}


	@Test
	public void testUnhappy()
		throws Exception {

		LDAPHealthCheck healthCheck = new LDAPHealthCheck(testLDAPServer, new DN("ou=no-such-entry,dc=wonderland,dc=net"), null);

		HealthCheck.Result result = healthCheck.check();

		assertFalse(result.isHealthy());
		assertEquals("LDAP connector health check failure: Missing test entry: ou=no-such-entry,dc=wonderland,dc=net", result.getMessage());
	}
}
