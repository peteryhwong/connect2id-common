package com.nimbusds.common.config;


import java.util.Properties;

import junit.framework.TestCase;

import com.thetransactioncompany.util.PropertyParseException;


/**
 * Tests the custom trust store configuration.
 */
public class CustomTrustStoreConfigurationTest extends TestCase {
	
	
	public static Properties getProperties() {
	
		Properties props = new Properties();
		
		props.setProperty("json2ldap.customTrustStore.enable", "true");
		props.setProperty("json2ldap.customTrustStore.file", "/home/json2ldap/keystore.jks");
		props.setProperty("json2ldap.customTrustStore.type", "JKS");
		props.setProperty("json2ldap.customTrustStore.password", "secret");
		
		return props;
	}
	
	
	public void testConfiguration()
		throws Exception {
	
		Properties props = getProperties();
	
		CustomTrustStoreConfiguration customTrustStore = null;
		
		String prefix = "json2ldap.customTrustStore.";
		
		try {
			customTrustStore = new CustomTrustStoreConfiguration(prefix, props);
		
		} catch (PropertyParseException e) {
			fail(e.getMessage());
		}
		
		assertTrue(customTrustStore.enable);
		assertEquals("/home/json2ldap/keystore.jks", customTrustStore.file);
		assertEquals("JKS", customTrustStore.type);
		assertEquals("secret", customTrustStore.password);
	}
}




