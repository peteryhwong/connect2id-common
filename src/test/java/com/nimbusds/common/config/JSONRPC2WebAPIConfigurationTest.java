package com.nimbusds.common.config;


import java.util.*;

import junit.framework.TestCase;

import com.thetransactioncompany.util.*;


/**
 * Tests the JSON-RPC 2.0 web API configuration class.
 */
public class JSONRPC2WebAPIConfigurationTest extends TestCase {


	public void testConfigDefaults() {
		
		Properties props = new Properties();
		
		JSONRPC2WebAPIConfiguration config = null;

		try {
			config = new JSONRPC2WebAPIConfiguration("abc.api.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}
		
		assertEquals("application/json; charset=utf-8", config.responseContentType);
		assertFalse(config.exposeExceptions);
		assertFalse(config.reportRequestProcTime);

		// test log
		config.log();
	}


	public void testConfigWithOptionalParams() {
		
		Properties props = new Properties();

		props.setProperty("abc.api.responseContentType", "text/plain; charset=utf-8");
		props.setProperty("abc.api.exposeExceptions", "true");
		props.setProperty("abc.api.reportRequestProcTime", "true");
		
		JSONRPC2WebAPIConfiguration config = null;

		try {
			config = new JSONRPC2WebAPIConfiguration("abc.api.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals("text/plain; charset=utf-8", config.responseContentType);
		assertTrue(config.exposeExceptions);
		assertTrue(config.reportRequestProcTime);

		// test log
		config.log();
	}
}
