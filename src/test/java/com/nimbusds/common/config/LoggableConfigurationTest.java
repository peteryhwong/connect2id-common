package com.nimbusds.common.config;


import junit.framework.TestCase;


/**
 * Tests the loggable configuration interface.
 */
public class LoggableConfigurationTest extends TestCase {


	public void testLogCategory() {

		assertEquals("MAIN", LoggableConfiguration.LOG_CATEGORY);
	}
}
