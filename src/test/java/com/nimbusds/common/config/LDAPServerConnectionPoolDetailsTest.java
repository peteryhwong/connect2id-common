package com.nimbusds.common.config;


import java.util.*;

import junit.framework.TestCase;

import com.thetransactioncompany.util.*;

import com.nimbusds.common.ldap.LDAPConnectionSecurity;


/**
 * Tests the LDAP server connection pool details configuration.
 */
public class LDAPServerConnectionPoolDetailsTest extends TestCase {


	public void testConfigDefaults() {
		
		Properties props = new Properties();

		props.setProperty("abc.ldapServer.url", "ldap://localhost:1389");
		
		LDAPServerConnectionPoolDetails config = null;

		try {
			config = new LDAPServerConnectionPoolDetails("abc.ldapServer.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals(1, config.url.length);
		assertEquals("ldap://localhost:1389", config.url[0].toString());

		// defaults
		assertNull(config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(0, config.connectTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals(5, config.connectionPoolSize);
		assertEquals(500, config.connectionPoolMaxWaitTime);
		assertEquals(0, config.connectionMaxAge);

		// test log
		config.log();
	}


	public void testMissingURLException() {

		Properties props = new Properties();

		try {
			new LDAPServerConnectionPoolDetails("abc.ldapServer.", props);

			fail("Failed to raise missing LDAP URL exception [1]");

		} catch (PropertyParseException e) {

			// ok
		}

		boolean requireURL = true;

		try {
			new LDAPServerConnectionPoolDetails("abc.ldapServer.", props, requireURL);

			fail("Failed to raise missing LDAP URL exception [2]");

		} catch (PropertyParseException e) {

			// ok
		}
	}


	public void testConfigWithOptionalParams() {
		
		Properties props = new Properties();

		props.setProperty("abc.ldapServer.url", "ldap://localhost:1389 ldap://remotehost:1389");
		props.setProperty("abc.ldapServer.selectionAlgorithm", "ROUND_ROBIN");
		props.setProperty("abc.ldapServer.security", "NONE");
		props.setProperty("abc.ldapServer.connectTimeout", "250");
		props.setProperty("abc.ldapServer.trustSelfSignedCerts", "true");
		props.setProperty("abc.ldapServer.connectionPoolSize", "20");
		props.setProperty("abc.ldapServer.connectionPoolMaxWaitTime", "1000");
		props.setProperty("abc.ldapServer.connectionMaxAge", "3600000");

		
		LDAPServerConnectionPoolDetails config = null;

		try {
			config = new LDAPServerConnectionPoolDetails("abc.ldapServer.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals(2, config.url.length);
		assertEquals("ldap://localhost:1389", config.url[0].toString());
		assertEquals("ldap://remotehost:1389", config.url[1].toString());

		// defaults
		assertEquals(ServerSelectionAlgorithm.ROUND_ROBIN, config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.NONE, config.security);
		assertEquals(250, config.connectTimeout);
		assertTrue(config.trustSelfSignedCerts);
		assertEquals(20, config.connectionPoolSize);
		assertEquals(1000, config.connectionPoolMaxWaitTime);
		assertEquals(3600000l, config.connectionMaxAge);

		// test log
		config.log();
	}
}
