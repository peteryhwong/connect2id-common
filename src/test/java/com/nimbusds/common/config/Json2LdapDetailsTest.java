package com.nimbusds.common.config;


import java.util.*;

import junit.framework.TestCase;

import com.thetransactioncompany.util.*;


/**
 * Tests the Json2Ldap service details configuration.
 */
public class Json2LdapDetailsTest extends TestCase {

	
	public void testConfigDefaults() {
		
		Properties props = new Properties();

		props.setProperty("abc.json2ldap.url", "http://localhost:8080/json2ldap/");
		
		Json2LdapDetails config = null;

		try {
			config = new Json2LdapDetails("abc.json2ldap.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals("http://localhost:8080/json2ldap/", config.url.toString());

		// defaults
		assertTrue(config.useDefaultLDAPServer);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);

		// test log
		config.log();
	}


	public void testConfigWithOptionalParams() {
		
		Properties props = new Properties();

		props.setProperty("abc.json2ldap.url", "http://localhost:8080/json2ldap/");
		props.setProperty("abc.json2ldap.useDefaultLDAPServer", "false");
		props.setProperty("abc.json2ldap.trustSelfSignedCerts", "true");
		props.setProperty("abc.json2ldap.connectTimeout", "250");
		props.setProperty("abc.json2ldap.readTimeout", "500");

		
		Json2LdapDetails config = null;

		try {
			config = new Json2LdapDetails("abc.json2ldap.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals("http://localhost:8080/json2ldap/", config.url.toString());

		// non-defaults
		assertFalse("Default LDAP server", config.useDefaultLDAPServer);
		assertTrue("Self-signed certs", config.trustSelfSignedCerts);
		assertEquals(250, config.connectTimeout);
		assertEquals(500, config.readTimeout);

		// test log
		config.log();
	}
}
